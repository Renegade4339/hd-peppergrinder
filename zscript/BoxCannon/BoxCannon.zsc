// ------------------------------------------------------------
// [BC24] L&M Box Cannon C24 Pistol Carbine
// ------------------------------------------------------------
/*  Initially produced by Lucarde & Morrison for a private security company's arms
contract for a lightweight but accurate pistol, the Box Cannon C24 Pistol Carbine
became the reliable sidearm of Belmondo special operatives during the war against
the Conluz cultists, former followers of apocalyptic prohet Jones Graham.

  It was when Conluz began summoning large swathes of demons in a bid for power
that the Box Cannon found it's most use as it was distributed into the hands of
the elite W.I.N.G. (Wallachian Infiltration Night Guards) Unit, where it was most
popular with certain agents who claim it could "fire bullets harder" and "surpass
any other pistol choice" despite being confronted with logical evidence to the
contrary. The commonly paired magazine extension, lightweight dual purpose
holster/stock, and stripper clips made it a reliable and sturdy weapon that is
easy to hastily load from salvaged rounds deep behind enemy lines. 

  Despite allegations of supplying both sides with alarming amounts of 9mm
munitions for profit, the Green Hawk Ammunition Company only commented "We never
distributed any to that region, we're fairly certain it's just one scary kid
wearing bellbottom jeans. It's fine, don't worry about it."*/


class HDBoxCannon:HDHandgun{
	default{
		+hdweapon.fitsinbackpack
		+hdweapon.reverseguninertia
		scale 0.63;
		weapon.selectionorder 50;
		weapon.slotnumber 2;
		weapon.slotpriority 1.8;
		weapon.kickback 30;
		weapon.bobrangex 0.1;
		weapon.bobrangey 0.6;
		weapon.bobspeed 2.5;
		weapon.bobstyle "normal";
		obituary "%o got blitzed by %k's Box Cannon.";
		inventory.pickupmessage "You got the Box Cannon 9mm!";
		tag "Box Cannon 9mm";
		hdweapon.refid HDLD_MAUSER;
		hdweapon.barrelsize 21,0.3,0.5;
		inventory.maxamount 3;
		
		hdweapon.loadoutcodes "
			\custock - 0/1, Broomhandle Stock
			\cuxmag - 0/1, Extended Magazine
			\cubarrel - 0/1, Carbine Barrel w/ Foregrip
			\cudrum - 0/1, Drum Magazine
			\cusilencer - 0/1, Mini-Tsuchi Silencer"; //Miniaturized Tsuchinoko Dual Purpose Silencer
	}
	override string pickupmessage(){
		if(weaponstatus[0]&MPSF_CAPITAN)return string.format("You discovered an El Capitan. Watch out for wolves.",super.pickupmessage());
		else if(weaponstatus[0]&MPSF_STOCK&&weaponstatus[MPSS_MAGTYPE]==1&&weaponstatus[MPSS_BARREL]==1)return string.format("%s With the works.",super.pickupmessage());
		else if(weaponstatus[0]&MPSF_STOCK&&weaponstatus[MPSS_MAGTYPE]==1)return string.format("%s Leon's Choice!",super.pickupmessage());
		else if(weaponstatus[0]&MPSF_STOCK&&weaponstatus[MPSS_BARREL]==1)return string.format("%s Full Rifle Configuration.",super.pickupmessage());
		else if(weaponstatus[MPSS_MAGTYPE]==1&&weaponstatus[MPSS_BARREL]==1)return string.format("%s Chicago Setup.",super.pickupmessage());
		else if(weaponstatus[0]&MPSF_STOCK)return string.format("%s Complete with Broomhandle stock.",super.pickupmessage());
		else if(weaponstatus[MPSS_MAGTYPE]==1)return string.format("%s With double the magazine.",super.pickupmessage());
		else if(weaponstatus[MPSS_BARREL]==1)return string.format("%s With rifle-style barrel.",super.pickupmessage());
		return super.pickupmessage();

	}
	override double weaponbulk(){
		int stk=-1;
		if ( //ONLY stock attatched
			weaponstatus[0]&MPSF_STOCK
			&&!(
				weaponstatus[MPSS_MAGTYPE]>0
				||weaponstatus[MPSS_BARREL]>0
				)
			)stk=4;
		else if (weaponstatus[0]&MPSF_STOCK)stk=24;
		else stk=0;
		int xmg=-1;
		if (weaponstatus[MPSS_MAGTYPE]==1)xmg=18;
		else xmg=0;
		int xbl=-1;
		if (weaponstatus[MPSS_BARREL]==1)xbl=18;
		else xbl=0;
		int drm=-1;
		if (weaponstatus[MPSS_MAGTYPE]==2)drm=36;
		else drm=0;
		int mgg=weaponstatus[MPSS_MAG];
		return 40+stk+xmg+xbl+drm+(mgg<0?0:(mgg*ENC_9_LOADED));
	}
	override double gunmass(){
		int stk=-1;
		if (weaponstatus[0]&MPSF_STOCK)stk=1;
		else stk=0;
		int xmg=-1;
		if (weaponstatus[MPSS_MAGTYPE]==1)xmg=1;
		else xmg=0;
		int xbl=-1;
		if (weaponstatus[MPSS_BARREL]==1)xbl=2;
		else xbl=0;
		int drm=-1;
		if (weaponstatus[MPSS_BARREL]==1)drm=6;
		else drm=0;
		int hnd=-1;
		if (weaponstatus[0]&MPSF_CAPITAN)hnd=6;
		return 10+stk+xmg+xbl+hnd+drm;
	}
	override void postbeginplay(){
		super.postbeginplay();
		if(!GetCvar("sv_cheats"))weaponstatus[0]&=~MPSF_CHEAT; //cheatmode only
		if(weaponstatus[0]&MPSF_STOCK&&weaponstatus[MPSS_BARREL]==1)bfitsinbackpack=false;
		if(weaponstatus[0]&MPSF_CAPITAN){
			barrellength=69;
			bfitsinbackpack=false;
		}else if(weaponstatus[MPSS_BARREL]==1){
			if(weaponstatus[0]&MPSF_STOCK)barrellength=30;
			else barrellength=26;
		}else if(weaponstatus[0]&MPSF_STOCK)barrellength=24;
		else barrellength=21;
		if(weaponstatus[MPSS_BARREL]==1)weaponspecial=1337;
	}
	override void failedpickupunload(){
		failedpickupunloadmag(MPSS_MAG,"HD9mClip");
	}
	override string,double getpickupsprite(){
		string spr;
		int mgt=weaponstatus[MPSS_MAGTYPE];
		int brt=weaponstatus[MPSS_BARREL];
		bool stk=weaponstatus[0]&MPSF_STOCK;
		if(!stk){
			if(brt==0){
				if(mgt==0)spr="A";
				if(mgt==1)spr="C";
				if(mgt==2)spr="I";
			}if(brt==1){
				if(mgt==0)spr="D";
				if(mgt==1)spr="G";
				if(mgt==2)spr="K";
			}if(brt==2){
				if(mgt==0)spr="M";
				if(mgt==1)spr="O";
				if(mgt==2)spr="P";
			}
		}
		if(stk){
			if(brt==0){
				if(mgt==0)spr="B";
				if(mgt==1)spr="E";
				if(mgt==2)spr="J";
			}if(brt==1){
				if(mgt==0)spr="F";
				if(mgt==1)spr="H";
				if(mgt==2)spr="L";
			}if(brt==2){
				if(mgt==0)spr="N";
				if(mgt==1)spr="Q";
				if(mgt==2)spr="R";
			}
		}if(weaponstatus[0]&MPSF_CAPITAN)spr="Z";
		return "MPS1"..spr.."0",1.;
	}
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		int nineclip=hpl.countinv("HD9mClip");
		sb.drawimage("PRNDA0",(-45,-8),sb.DI_SCREEN_CENTER_BOTTOM,scale:(2.1,2.1));
		sb.drawnum(hpl.countinv("HDPistolAmmo"),-43,-8,sb.DI_SCREEN_CENTER_BOTTOM);
		if(nineclip>0){
			int nextmagloaded=sb.GetNextLoadMag(hdmagammo(hpl.findinventory("HD9mClip")));
			if(nextmagloaded<1){
				sb.drawimage("9CLPF0",(-64,-3),sb.DI_SCREEN_CENTER_BOTTOM,alpha:nextmagloaded?0.6:1.,scale:(1.6,1.6));
			}else if(nextmagloaded<3){
				sb.drawimage("9CLPE0",(-64,-3),sb.DI_SCREEN_CENTER_BOTTOM,scale:(1.6,1.6));
			}else if(nextmagloaded<5){
				sb.drawimage("9CLPD0",(-64,-3),sb.DI_SCREEN_CENTER_BOTTOM,scale:(1.6,1.6));
			}else if(nextmagloaded<7){
				sb.drawimage("9CLPC0",(-64,-3),sb.DI_SCREEN_CENTER_BOTTOM,scale:(1.6,1.6));
			}else if(nextmagloaded<9){
				sb.drawimage("9CLPB0",(-64,-3),sb.DI_SCREEN_CENTER_BOTTOM,scale:(1.6,1.6));
			}else sb.drawimage("9CLPA0",(-64,-3),sb.DI_SCREEN_CENTER_BOTTOM,scale:(1.6,1.6));
			sb.drawnum(hpl.countinv("HD9mClip"),-60,-8,sb.DI_SCREEN_CENTER_BOTTOM);
		}
		if(hdw.weaponstatus[MPSS_MAGTYPE]==2){
			int mag=hdw.weaponstatus[MPSS_MAG];
			sb.drawwepnum(clamp(mag,0,20),20);
			sb.drawwepnum(clamp(mag-20,0,20),20,posy:-2);
		}else if(hdw.weaponstatus[MPSS_MAGTYPE]==1)sb.drawwepnum(hdw.weaponstatus[MPSS_MAG],20);
		else(sb.drawwepnum(hdw.weaponstatus[MPSS_MAG],10));
		if(hdw.weaponstatus[MPSS_CHAMBER]==2)sb.drawwepdot(-16,-10,(3,1));
		sb.drawwepcounter(hdw.weaponstatus[0]&MPSF_FIREMODE,
			-22,-10,"RBRSA3A7","STBURAUT"
		);
/*		if(hdw.weaponstatus[0]&MPSF_CHEAT)
		{
			sb.drawstring(
				sb.mhudfont,"RED9",(-30,-30),
				sb.DI_TEXT_ALIGN_RIGHT|sb.DI_TRANSLATABLE|sb.DI_SCREEN_CENTER_BOTTOM,
				Font.CR_DARKRED
			);
		}
*/	}
	override string gethelptext(){
		return
		WEPHELP_FIRESHOOT
		..WEPHELP_FIREMODE.."  Semi/Auto\n"
		..WEPHELP_RELOAD.." Reload".." (+ "..WEPHELP_FIREMODE.."  Load Loose 9mm)\n"
		..WEPHELP_UNLOADUNLOAD
		..WEPHELP_ALTRELOAD.."  Quick-Swap (if available)\n"
		..WEPHELP_MAGMANAGER..
		((weaponstatus[MPSS_BARREL]>0||weaponstatus[0]&MPSF_STOCK||weaponstatus[MPSS_MAGTYPE]>0)?
		"\nInstalled Upgrades: ":"") //only visible when an attatchment is present
		..(weaponstatus[MPSS_BARREL]==1?"\n- Extended Barrel w/ Foregrip":"")
		..(weaponstatus[MPSS_BARREL]==2?"\n- Mini-Tsuchi Dual Purpose Silencer":"")
		..(weaponstatus[0]&MPSF_STOCK?"\n- Broomhandle Stock / Holster":"")
		..(weaponstatus[MPSS_MAGTYPE]==1?"\n- Extended Internal Magazine":"")
		..(weaponstatus[MPSS_MAGTYPE]==2?"\n- Autowinding Drum Magazine":"")
		;
	}
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc,string whichdot
	){
		int cx,cy,cw,ch;
		[cx,cy,cw,ch]=screen.GetClipRect();
		vector2 scc;
		vector2 bobb=bob*1.3;

		//if slide is pushed back, throw sights off line
		if(hpl.player.getpsprite(PSP_WEAPON).frame>=2){
			sb.SetClipRect(
				-10+bob.x,-5+bob.y,20,14,
				sb.DI_SCREEN_CENTER
			);
			scc=(0.7,0.8);
			bobb.y=clamp(bobb.y*1.1-3,-10,10);
		}else{
			sb.SetClipRect(
				-8+bob.x,-4+bob.y,16,10,
				sb.DI_SCREEN_CENTER
			);
			scc=(0.6,0.6);
			bobb.y=clamp(bobb.y,-8,8);
		}
		sb.drawimage(
			"MauFS",(0,0)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			alpha:0.9,scale:scc
		);
		sb.SetClipRect(cx,cy,cw,ch);
		sb.drawimage(
			"MauBS",(0,-1)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			scale:scc
		);
	}
	override void DropOneAmmo(int amt){
		if(owner){
			amt=clamp(amt,1,10);
			if(owner.countinv("HD9mClip"))owner.A_DropInventory("HD9mClip",amt);
			else owner.A_DropInventory("HDPistolAmmo",amt*10);
		}
	}
	override void initializewepstats(bool idfa){
		if(weaponstatus[MPSS_MAGTYPE]==2)weaponstatus[MPSS_MAG]=40;
		else if(weaponstatus[MPSS_MAGTYPE]==1)weaponstatus[MPSS_MAG]=20;
		else weaponstatus[MPSS_MAG]=10;
		weaponstatus[MPSS_CHAMBER]=2;
	}
	override void ForceBasicAmmo(){
		if(weaponstatus[MPSS_MAGTYPE]==2)owner.A_SetInventory("HDPistolAmmo",41);
		else if(weaponstatus[MPSS_MAGTYPE]==1)owner.A_SetInventory("HDPistolAmmo",21);
		else owner.A_SetInventory("HDPistolAmmo",11);
	}
	action void A_CheckPistolHand(){ //Now a subset of A_BCSpriteIndex
		if(invoker.wronghand){
			if(invoker.weaponstatus[MPSS_MAGTYPE]>1){
				if(invoker.weaponstatus[MPSS_BARREL]==2)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("MAP2A0");
				if(invoker.weaponstatus[MPSS_BARREL]==1)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("MAK2A0");
				if(invoker.weaponstatus[MPSS_BARREL]==0)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("MAI2A0");
			}else{
				if(invoker.weaponstatus[MPSS_BARREL]==2)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("MAM2A0");
				if(invoker.weaponstatus[MPSS_BARREL]==1)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("MAD2A0");
				if(invoker.weaponstatus[MPSS_BARREL]==0)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("MAU2A0");
			}
		}
	}
	action void A_BCSpriteIndex(){ //Take precident over checking hand
		if(invoker.weaponstatus[0]&MPSF_STOCK){
			if(invoker.weaponstatus[MPSS_MAGTYPE]>1){
				if(invoker.weaponstatus[MPSS_BARREL]==2)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("MAURA0");
				if(invoker.weaponstatus[MPSS_BARREL]==1)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("MAULA0");
				if(invoker.weaponstatus[MPSS_BARREL]==0)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("MAUJA0");
			}else{
				if(invoker.weaponstatus[MPSS_BARREL]==2)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("MAUNA0");
				if(invoker.weaponstatus[MPSS_BARREL]==1)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("MAUFA0");
				if(invoker.weaponstatus[MPSS_BARREL]==0)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("MAUBA0");
			}
		}else{
			if(invoker.weaponstatus[MPSS_MAGTYPE]>1){
				if(invoker.weaponstatus[MPSS_BARREL]==2)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("MAUPA0");
				if(invoker.weaponstatus[MPSS_BARREL]==1)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("MAUKA0");
				if(invoker.weaponstatus[MPSS_BARREL]==0)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("MAUIA0");
			}else{
				if(invoker.weaponstatus[MPSS_BARREL]==2)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("MAUMA0");
				if(invoker.weaponstatus[MPSS_BARREL]==1)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("MAUDA0");
				if(invoker.weaponstatus[MPSS_BARREL]==0)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("MAUSA0");
			}
			A_CheckPistolHand();
		}
	}
	states{
	precache:	//Sprites wont show up unless they are somewhere in a state.
		MAUS A 0; // MAUS is default, everything else uses pickup sprite letter.
		MAUB A 0;
		MAUD A 0;
		MAUF A 0;
		MAUI A 0;
		MAUJ A 0;
		MAUK A 0;
		MAUL A 0;
		MAUM A 0;
		MAUN A 0;
		MAUP A 0;
		MAUR A 0;
		MAU2 A 0; // Alt Hand versions
		MAD2 A 0;
		MAI2 A 0;
		MAK2 A 0;
		MAM2 A 0;
		MAP2 A 0;
		stop;
	select0:
		MAUS A 0{
			if(!countinv("NulledWeapon"))invoker.wronghand=false;
			A_TakeInventory("NulledWeapon");
//			A_CheckPistolHand();
			A_BCSpriteIndex();
		}
		#### A 0 A_JumpIf(invoker.weaponstatus[MPSS_CHAMBER]>0,2);
		#### C 0;
		---- A 1 A_Raise();
		---- A 1 A_Raise(30);
		---- A 1 A_Raise(30);
		---- A 1 A_Raise(24);
		---- A 1 A_Raise(18);
		wait;
	deselect0:
		MAUS A 0 A_BCSpriteIndex(); //A_CheckPistolHand();
		#### A 0 A_JumpIf(invoker.weaponstatus[MPSS_CHAMBER]>0,2);
		#### C 0;
		---- AAA 1 A_Lower();
		---- A 1 A_Lower(18);
		---- A 1 A_Lower(24);
		---- A 1 A_Lower(30);
		wait;

	ready:
		MAUS A 0 {
//			A_CheckPistolHand();
			A_BCSpriteIndex();
//			if(invoker.weaponstatus[MPSS_BARREL]==2)player.getpsprite(PSP_WEAPON).sprite=getspriteindex("MAUQA0");
		}
		#### A 0 {invoker.weaponstatus[MPSS_RATCHET]=0;}
		#### A 0 A_JumpIf(invoker.weaponstatus[MPSS_CHAMBER]>0,2);
		#### C 0 ;
		#### # 0 A_SetCrosshair(21);
		#### # 1 {A_WeaponReady(WRF_ALL);}
		goto readyend;
	user3:
		---- A 0 A_MagManager("HD9mClip");
		goto ready;
	user2:
	firemode:
		---- A 0{invoker.weaponstatus[0]^=MPSF_FIREMODE;}
	fmloop:
		---- A 1 A_JumpIf(PressingReload(),"reload");
		---- A 0 A_JumpIf(PressingFiremode(),"fmloop");
		goto nope;
	chamber_manual:
		---- A 0 A_JumpIf(
			!(invoker.weaponstatus[0]&MPSF_JUSTUNLOAD)
			&&(
				invoker.weaponstatus[MPSS_CHAMBER]==2
				||invoker.weaponstatus[MPSS_MAG]<1
			)
			,"nope"
		);
		#### B 3 offset(0,34);
		#### C 4 offset(0,37){
			A_MuzzleClimb(frandom(0.4,0.5),-frandom(0.6,0.8));
			A_StartSound("weapons/pismagclick",8);
			int mpch=invoker.weaponstatus[MPSS_CHAMBER];
			invoker.weaponstatus[MPSS_CHAMBER]=0;
			if(mpch==2){
				A_EjectCasing("HDPistolAmmo",
					frandom(-1,2),
					(frandom(0,0.2),-frandom(2,3),frandom(0.4,0.5)),
					(-2,0,-1)
				);
			}else if(mpch==1){
				A_EjectCasing("HDSpent9mm",
					-frandom(-1,2),
					(frandom(0.4,0.7),-frandom(6,7),frandom(0.8,1)),
					(-2,0,-1)
				);
			}
			if(invoker.weaponstatus[MPSS_MAG]>0){
				invoker.weaponstatus[MPSS_CHAMBER]=2;
				invoker.weaponstatus[MPSS_MAG]--;
			}
		}
		---- A 3 offset(0,35);
		goto nope;
	althold:
	hold:
		---- A 0{
			if(
				!(invoker.weaponstatus[0]&MPSF_FIREMODE)
				||invoker.weaponstatus[MPSS_RATCHET]>2
			)setweaponstate("nope");
		}goto fire;
	fire:
		#### A 0{
			invoker.weaponstatus[0]&=~MPSF_JUSTUNLOAD;
			if(invoker.weaponstatus[MPSS_CHAMBER]==2)setweaponstate("shoot");
			else if(invoker.weaponstatus[MPSS_MAG]>0)setweaponstate("chamber_manual");
		}goto nope;
	shoot:
		#### B 1;
		#### B 1{
			if(invoker.weaponstatus[MPSS_CHAMBER]==2)A_GunFlash();
		}
		#### C 1{
			if(hdplayerpawn(self)){
				hdplayerpawn(self).gunbraced=false;
				if(invoker.weaponstatus[0]&MPSF_STOCK){
					A_MuzzleClimb(
						-frandom(0.4,0.5),-frandom(0.6,0.8),
						frandom(0.2,0.3),frandom(0.3,0.4));
				}else{
					A_MuzzleClimb(
						-frandom(0.8,1.),-frandom(1.2,1.6),
						frandom(0.4,0.5),frandom(0.6,0.8));
					A_GiveInventory("IsMoving",1);
				}
			}
			A_EjectCasing("HDSpent9mm",
					-frandom(-1,2),
					(frandom(0.4,0.7),-frandom(6,7),frandom(0.8,1)),
					(-2,0,-1)
				);
			invoker.weaponstatus[MPSS_CHAMBER]=0;
			if(invoker.weaponstatus[MPSS_MAG]<1){
				A_StartSound("weapons/pistoldry",8,CHANF_OVERLAP,0.9);
				setweaponstate("nope");
			}
		}
		#### C 0{
			A_WeaponReady(WRF_NOFIRE);
			invoker.weaponstatus[MPSS_CHAMBER]=2;
			invoker.weaponstatus[MPSS_MAG]--;
			if(
				(invoker.weaponstatus[0]&MPSF_FIREMODE)
			){
				let pnr=HDPlayerPawn(self);
				if(
					pnr&&countinv("IsMoving")
					&&pnr.fatigue<12
				)pnr.fatigue++;
				if(!(invoker.weaponstatus[0]&MPSF_STOCK))A_GiveInventory("IsMoving",5);
				A_Refire("hold");
			}else A_Refire();
		}
		#### C 2;
		#### A 1;
		goto ready;
	flash:
//		MAU2 A 0 A_JumpIf(invoker.wronghand,2);
		---- A 0 {
			if(invoker.weaponstatus[MPSS_BARREL]==0)player.getpsprite(PSP_FLASH).sprite=getspriteindex("MAUSA0");
			if(invoker.weaponstatus[MPSS_BARREL]==1)player.getpsprite(PSP_FLASH).sprite=getspriteindex("MAUDA0");
			if(invoker.weaponstatus[MPSS_BARREL]==2)player.getpsprite(PSP_FLASH).sprite=getspriteindex("TNT1A0");
		}
		#### F 1 bright{
			HDFlashAlpha(64);
			A_Light1();
			double bcspd=1.14;
			double bcspr=2.0;
			if(invoker.weaponstatus[MPSS_BARREL]==1){bcspd=1.42;bcspr=1.24;}
			if(
				invoker.weaponstatus[0]&MPSF_CAPITAN
				||invoker.weaponstatus[0]&MPSF_CHEAT
				){bcspd=4.20;bcspr=0.69;}  //Nice.
			let bbb=HDBulletActor.FireBullet(self,"HDB_9",spread:bcspr,speedfactor:bcspd);
			if(
				frandom(0,ceilingz-floorz)<bbb.speed*0.3
			){
				if(invoker.weaponstatus[MPSS_BARREL]==2)A_AlertMonsters(24);
				else A_AlertMonsters(320);
			}

			invoker.weaponstatus[MPSS_RATCHET]++;
			invoker.weaponstatus[MPSS_CHAMBER]=1;
			if(invoker.weaponstatus[0]&MPSF_STOCK){
				A_ZoomRecoil(0.955);
			}else{
				A_ZoomRecoil(0.895);
				A_MuzzleClimb(-frandom(-0.8,1.2),-frandom(0.8,1.2));
			}
		}
		---- A 0{
			if(invoker.weaponstatus[MPSS_BARREL]==2)A_StartSound("weapons/bcquiet",CHAN_WEAPON);
			else A_StartSound("weapons/bcfire",CHAN_WEAPON);
			}
		---- A 0 A_Light0();
		---- A 0 {if(invoker.weaponstatus[0]&MPSF_CAPITAN)DropInventory(invoker);}
		stop;

	unload:
		---- A 1 offset(0,34);
		---- A 1 offset(2,36);
		---- A 1 offset(4,40);
		---- A 2 offset(8,42){
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			A_StartSound("weapons/rifleclick2",8);
		}
		---- A 4 offset (14,46){
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			A_StartSound("weapons/rifleload",8);
		}
		---- A 0{
			invoker.weaponstatus[0]|=MPSF_JUSTUNLOAD;
			if(invoker.weaponstatus[MPSS_MAG]>0)setweaponstate("unloadloop");
		}goto chamber_manual;
	unloadloop:
		---- A 4 offset(3,41){
			if(invoker.weaponstatus[MPSS_MAG]<1)setweaponstate("unloaddone");
			else{
				A_StartSound("weapons/rifleclick2",8);
				invoker.weaponstatus[MPSS_MAG]--;
				if(A_JumpIfInventory("HDPistolAmmo",0,"null")){
					A_SpawnItemEx(
						"HDPistolAmmo",cos(pitch)*8,0,height-7-sin(pitch)*8,
						cos(pitch)*cos(angle-40)*1+vel.x,
						cos(pitch)*sin(angle-40)*1+vel.y,
						-sin(pitch)*1+vel.z,
						0,SXF_ABSOLUTEMOMENTUM|
						SXF_NOCHECKPOSITION|SXF_TRANSFERPITCH
					);
				}else A_GiveInventory("HDPistolAmmo",1);
			}
		}
		---- A 2 offset(2,42);
		---- A 0{
			if(
				PressingReload()||
				PressingFire()||
				PressingAltFire()
			)setweaponstate("unloaddone");
		}loop;
	unloaddone:
		---- A 2 offset(2,42);
		---- A 3 offset(3,41);
		---- A 1 offset(4,40) A_StartSound("weapons/rifleclick",8);
		---- A 1 offset(2,36);
		---- A 1 offset(0,34);
		goto ready;
	reload:
		---- A 0{invoker.weaponstatus[0]&=~MPSF_DONTUSECLIPS;}
		goto reloadstart;
	reloadstart:
		---- A 1 offset(0,34);
		---- A 1 offset(2,36);
		---- A 1 offset(4,40);
		---- A 2 offset(8,42){
			A_StartSound("weapons/rifleclick2",8,CHANF_OVERLAP,0.9,pitch:0.95);
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
			if(PressingFiremode())invoker.weaponstatus[0]|=MPSF_DONTUSECLIPS;
		}
		---- A 4 offset(14,46){
			A_StartSound("weapons/rifleload",8,CHANF_OVERLAP);
			A_MuzzleClimb(-frandom(0.4,0.8),frandom(0.4,1.4));
		}
		---- A 0{
			int mg=invoker.weaponstatus[MPSS_MAG];
			if((
				(mg==40)
				&&invoker.weaponstatus[MPSS_MAGTYPE]==2
			)||(
				(mg==20)
				&&invoker.weaponstatus[MPSS_MAGTYPE]==1
			)||(
				mg==10
				&&invoker.weaponstatus[MPSS_MAGTYPE]==0
			)){
				setweaponstate("reloaddone");
				return;
			}
			else if(invoker.weaponstatus[0]&MPSF_DONTUSECLIPS)setweaponstate("loadhand");
			else if(
				(
					mg<1
					||(mg<11&&invoker.weaponstatus[MPSS_MAGTYPE]==1)
					||(mg<31&&invoker.weaponstatus[MPSS_MAGTYPE]==2)
					||!countinv("HDPistolAmmo")
				)&&!HDMagAmmo.NothingLoaded(self,"HD9mClip")
			)setweaponstate("loadclip");
		}
	loadhand:
		---- A 0 A_JumpIfInventory("HDPistolAmmo",1,"loadhandloop");
		goto reloaddone;
	loadhandloop:
		---- A 4{
			int rldmx=-1;
			if(invoker.weaponstatus[MPSS_MAGTYPE]==2)rldmx=40;
			else if(invoker.weaponstatus[MPSS_MAGTYPE]==1)rldmx=20;
			else rldmx=10;
			int hnd=min(
				countinv("HDPistolAmmo"),3,
				rldmx-invoker.weaponstatus[MPSS_MAG]
			);
			if(hnd<1){
				setweaponstate("reloaddone");
				return;
			}else{
				A_TakeInventory("HDPistolAmmo",hnd,TIF_NOTAKEINFINITE);
				invoker.weaponstatus[MPSS_HAND]=hnd;
				if(invoker.weaponstatus[MPSS_CHAMBER]<2){
					invoker.weaponstatus[MPSS_MAG]--;
					invoker.weaponstatus[MPSS_CHAMBER]=2;
				}
				A_StartSound("weapons/pocket",9);
			}
		}
	loadone:
		---- A 2 offset(16,50) A_JumpIf(invoker.weaponstatus[MPSS_HAND]<1,"loadhandnext");
		---- A 4 offset(14,46){
			invoker.weaponstatus[MPSS_HAND]--;
			invoker.weaponstatus[MPSS_MAG]++;
			if(invoker.weaponstatus[MPSS_CHAMBER]<2){
					invoker.weaponstatus[MPSS_MAG]--;
					invoker.weaponstatus[MPSS_CHAMBER]=2;
				}
			A_StartSound("weapons/rifleclick2",8);
		}loop;
	loadhandnext:
		---- A 8 offset(16,48){
			if(
				PressingUnload()||
				PressingFire()||
				PressingAltFire()||
				!PressingReload()||
				!countinv("HDPistolAmmo")	//don't strip clips automatically
				)setweaponstate("reloaddone");
				else A_StartSound("weapons/pocket",9);
			}goto loadhandloop;
	loadclip:
		---- A 0{
			if((
					(invoker.weaponstatus[MPSS_MAGTYPE]==2)
					&&(invoker.weaponstatus[MPSS_MAG]>39)
				)||(
					(invoker.weaponstatus[MPSS_MAGTYPE]==1)
					&&(invoker.weaponstatus[MPSS_MAG]>19)
				)||(
					!(invoker.weaponstatus[MPSS_MAGTYPE]==1
						||invoker.weaponstatus[MPSS_MAGTYPE]==2)
					&&invoker.weaponstatus[MPSS_MAG]>9
				)){
					setweaponstate("reloaddone");
					return;
				}
			}
		---- A 1 offset(16,50){
			let ccc=hdmagammo(findinventory("HD9mClip"));
			if(ccc){
				//find the last mag that has anything in it and load from that
				bool fullmag=false;
				int magindex=-1;
				for(int i=ccc.mags.size()-1;i>=0;i--){
					if(ccc.mags[i]>=10)fullmag=true;
					if(magindex<0&&ccc.mags[i]>0)magindex=i;
					if(fullmag&&magindex>0)break;
				}
				if(magindex<0){
					setweaponstate("reloaddone");
					return;
				}

				//load the whole clip at once if possible
				if((
					fullmag
					&&invoker.weaponstatus[MPSS_MAGTYPE]==2
					&&invoker.weaponstatus[MPSS_MAG]<31
				)||(
					fullmag
					&&invoker.weaponstatus[MPSS_MAGTYPE]==1
					&&!(invoker.weaponstatus[MPSS_MAGTYPE]==2)
					&&invoker.weaponstatus[MPSS_MAG]<11
				)||(
					fullmag
					&&!(invoker.weaponstatus[MPSS_MAGTYPE]==1
						||invoker.weaponstatus[MPSS_MAGTYPE]==2)
					&&invoker.weaponstatus[MPSS_MAG]<1
				)){
					setweaponstate("loadwholeclip");
					return;
				}

				//strip one round and load it
				A_StartSound("weapons/rifleclick2",CHAN_WEAPONBODY);
				invoker.weaponstatus[MPSS_MAG]++;
				ccc.mags[magindex]--;
				if(invoker.weaponstatus[MPSS_CHAMBER]<2){
					invoker.weaponstatus[MPSS_MAG]--;
					invoker.weaponstatus[MPSS_CHAMBER]=2;
				}
			}
		}
		---- A 5 offset(16,52) A_JumpIf(
			PressingReload()||
			PressingFire()||
			PressingAltFire()||
			PressingZoom()
		,"reloaddone");
		loop;
	loadwholeclip:
		---- A 3 offset(16,50) A_StartSound("weapons/rifleclick2",8);
		---- AAA 2 offset(17,52) A_StartSound("weapons/rifleclick2",8,pitch:1.01);
		---- AAA 1 offset(16,50) A_StartSound("weapons/rifleclick2",8,CHANF_OVERLAP,pitch:1.02);
		---- AAA 1 offset(15,48) A_StartSound("weapons/rifleclick2",8,CHANF_OVERLAP,pitch:1.02);
		---- A 2 offset(14,46){
			A_StartSound("weapons/rifleclick",CHAN_WEAPONBODY);
			let ccc=hdmagammo(findinventory("HD9mClip"));
			if(ccc){
				invoker.weaponstatus[MPSS_MAG]=invoker.weaponstatus[MPSS_MAG]+10;
				if(invoker.weaponstatus[MPSS_CHAMBER]<2){
					invoker.weaponstatus[MPSS_MAG]--;
					invoker.weaponstatus[MPSS_CHAMBER]=2;
				}
				ccc.TakeMag(true);
				if(pressingreload()){
					ccc.addamag(0);
					A_SetTics(10);
					A_StartSound("weapons/pocket",CHAN_POCKETS);
				}else HDMagAmmo.SpawnMag(self,"HD9mClip",0);
			}
		}goto reloaddone;
	reloaddone:
		---- A 1 offset(4,40);
		---- A 1 offset(2,36);
		---- A 1 offset(0,34);
		goto nope;
	user1:
	altreload:
	swappistols:
		---- A 0 A_JumpIf((
			invoker.weaponstatus[0]&MPSF_STOCK
			||invoker.weaponstatus[0]&MPSF_CAPITAN
			),"nope"); //stock and bullshit requires two hands
		---- A 0 A_SwapHandguns();
		---- A 0{
			bool id=(Wads.CheckNumForName("id",0)!=-1);
			bool offhand=invoker.wronghand;
			bool lefthanded=(id!=offhand);
			if(lefthanded){
				A_Overlay(1025,"raiseleft");
				A_Overlay(1026,"lowerright");
			}else{
				A_Overlay(1025,"raiseright");
				A_Overlay(1026,"lowerleft");
			}
		}
		TNT1 A 5;
		MAUS A 0 A_BCSpriteIndex(); //A_CheckPistolHand();
		goto nope;
	lowerleft:
		MAUS A 0 A_JumpIf(Wads.CheckNumForName("id",0)!=-1,2);
		MAU2 A 0;
		#### B 1 offset(-6,38);
		#### B 1 offset(-12,48);
		#### B 1 offset(-20,60);
		#### B 1 offset(-34,76);
		#### B 1 offset(-50,86);
		stop;
	lowerright:
		MAU2 A 0 A_JumpIf(Wads.CheckNumForName("id",0)!=-1,2);
		MAUS A 0;
		#### B 1 offset(6,38);
		#### B 1 offset(12,48);
		#### B 1 offset(20,60);
		#### B 1 offset(34,76);
		#### B 1 offset(50,86);
		stop;
	raiseleft:
		MAUS A 0 A_JumpIf(Wads.CheckNumForName("id",0)!=-1,2);
		MAU2 A 0;
		#### A 1 offset(-50,86);
		#### A 1 offset(-34,76);
		#### A 1 offset(-20,60);
		#### A 1 offset(-12,48);
		#### A 1 offset(-6,38);
		stop;
	raiseright:
		MAU2 A 0 A_JumpIf(Wads.CheckNumForName("id",0)!=-1,2);
		MAUS A 0;
		#### A 1 offset(50,86);
		#### A 1 offset(34,76);
		#### A 1 offset(20,60);
		#### A 1 offset(12,48);
		#### A 1 offset(6,38);
		stop;
	whyareyousmiling:
		#### B 1 offset(0,48);
		#### B 1 offset(0,60);
		#### B 1 offset(0,76);
		TNT1 A 7;
		MAUS A 0{
			invoker.wronghand=!invoker.wronghand;
			A_BCSpriteIndex();
			//A_CheckPistolHand();
		}
		#### B 1 offset(0,76);
		#### B 1 offset(0,60);
		#### B 1 offset(0,48);
		goto nope;


	spawn:
		MPS1 ABCDEFGHIJKLYZ -1 nodelay{
			int mgt=invoker.weaponstatus[MPSS_MAGTYPE];
			int brt=invoker.weaponstatus[MPSS_BARREL];
			bool stk=invoker.weaponstatus[0]&MPSF_STOCK;
			if(!stk){
				if(brt==0){
					if(mgt==0)frame=0;
					if(mgt==1)frame=2;
					if(mgt==2)frame=8;
				}if(brt==1){
					if(mgt==0)frame=3;
					if(mgt==1)frame=6;
					if(mgt==2)frame=10;
				}if(brt==2){
					if(mgt==0)frame=12;
					if(mgt==1)frame=14;
					if(mgt==2)frame=15;
				}
			}
			if(stk){
				if(brt==0){
					if(mgt==0)frame=24;
					if(mgt==1)frame=4;
					if(mgt==2)frame=9;
				}if(brt==1){
					if(mgt==0)frame=5;
					if(mgt==1)frame=7;
					if(mgt==2)frame=11;
				}if(brt==2){
					if(mgt==0)frame=13;
					if(mgt==1)frame=16;
					if(mgt==2)frame=17;
				}
			}if(invoker.weaponstatus[0]&MPSF_CAPITAN)frame=25;
		}stop;
	}
	override void loadoutconfigure(string input){
		int mstock=getloadoutvar(input,"stock",1);
		if(mstock>=0)weaponstatus[0]|=MPSF_STOCK;
		int mxmag=getloadoutvar(input,"xmag",1);
		if(mxmag>=1)weaponstatus[MPSS_MAGTYPE]=1;
		int mxbarrel=getloadoutvar(input,"barrel",1);
		if(mxbarrel>=1)weaponstatus[MPSS_BARREL]=1;
		int drum=getloadoutvar(input,"drum",1);
		if(drum>=1)weaponstatus[MPSS_MAGTYPE]=2;
		int silencer=getloadoutvar(input,"silencer",1);
		if(silencer>=1)weaponstatus[MPSS_BARREL]=2;
		int leon=getloadoutvar(input,"leon",1);
		if(leon>=0){
			weaponstatus[0]|=MPSF_STOCK;
			weaponstatus[MPSS_MAGTYPE]=1;
		}
		int carbine=getloadoutvar(input,"carbine",1);
		if(carbine>=0){
			weaponstatus[0]|=MPSF_STOCK;
			weaponstatus[MPSS_BARREL]=1;
		}
		int chicago=getloadoutvar(input,"chicago",1);
		if(chicago>=0){
			weaponstatus[MPSS_MAGTYPE]=2;
			weaponstatus[MPSS_BARREL]=1;
		}
		int repeater=getloadoutvar(input,"repeater",1);
		if(repeater>=0){
			weaponstatus[0]|=MPSF_STOCK;
			weaponstatus[MPSS_MAGTYPE]=1;
			weaponstatus[MPSS_BARREL]=1;
		}
		int assaulter=getloadoutvar(input,"assaulter",1);
		if(assaulter>=0){
			weaponstatus[0]|=MPSF_STOCK;
			weaponstatus[MPSS_MAGTYPE]=2;
			weaponstatus[MPSS_BARREL]=1;
		}
		int hund=getloadoutvar(input,"hund",1); //supposed to be secret, dont go blabbin, capiche?
		if(hund>=0){
			weaponstatus[0]|=MPSF_CAPITAN;
			weaponstatus[0]&=~MPSF_STOCK;
			weaponstatus[MPSS_MAGTYPE]=0;
			weaponstatus[MPSS_BARREL]=0;
			}
		if(weaponstatus[MPSS_MAGTYPE]==1)weaponstatus[MPSS_MAG]=20;
		if(weaponstatus[MPSS_MAGTYPE]==2)weaponstatus[MPSS_MAG]=40;
		int strongcheat=getloadoutvar(input,"cheat_iwannabeleonkennedy",1); //red9 go brrrrrr! Only works with sv_cheats enabled.
		if(strongcheat>=0)weaponstatus[0]|=MPSF_CHEAT;
	}
}
/*class HDMausStock:HDWeaponGiver{
	default{
		tag "Box Cannon w/ Stock";
		hdweapongiver.bulk 52+(MPSS_MAG<0?0:(MPSS_MAG*ENC_9_LOADED));
		hdweapongiver.weapontogive "HDBoxCannon";
		hdweapongiver.weprefid HDLD_MAUSTK;
		hdweapongiver.config "stock";
		inventory.icon "MPS1B0";
	}
}*/
class ElCapitan:HDWeaponGiver{ //dipshit werewolf nazi PoS gun
	default{
		tag "El Capitan";
		hdweapongiver.bulk 48+(MPSS_MAG<0?0:(MPSS_MAG*ENC_9_LOADED));
		hdweapongiver.weapontogive "HDBoxCannon";
		hdweapongiver.config "hund"; 
		inventory.icon "MPS1E0";
	}
}

enum mauserstatus{
	MPSF_FIREMODE=1,
	MPSF_JUSTUNLOAD=2,
	MPSF_DONTUSECLIPS=4,
	MPSF_STOCK=8,
	MPSF_CAPITAN=16,
	MPSF_CHEAT=32,
	
	MPSS_FLAGS=0,
	MPSS_MAG=1,
	MPSS_CHAMBER=2, //0 empty, 1 spent, 2 loaded
	MPSS_RATCHET=4,
	MPSS_HAND=5,
	MPSS_BARREL=6, //0 default, 1 xbarrel, 2 silencer
	MPSS_MAGTYPE=7, //0 default, 1 xmag, 2 drum
};

class BC24Spawn:IdleDummy{
	states{
	spawn:
		TNT1 A 0 nodelay{
			let zzz=HDBoxCannon(spawn("HDBoxCannon",pos,ALLOW_REPLACE));
			if(!zzz)return;
			HDF.TransferSpecials(self, zzz);
			if(!random(0,2)){
				zzz.weaponstatus[0]|=MPSF_STOCK;
			}if(!random(0,2)){
				if(!random(0,3)){
					zzz.weaponstatus[MPSS_MAGTYPE]=1;
					zzz.weaponstatus[MPSS_MAG]=20;
				}else{
					zzz.weaponstatus[MPSS_MAGTYPE]=2;
					zzz.weaponstatus[MPSS_MAG]=40;
				}
			}if(!random(0,4)){
				zzz.weaponstatus[MPSS_BARREL]=1;
			}
			spawn("HD9mClip",pos+(10,0,0),ALLOW_REPLACE);
			spawn("HD9mClip",pos+(8,0,0),ALLOW_REPLACE);
			spawn("HD9mClip",pos+(5,0,0),ALLOW_REPLACE);
			spawn("HD9mClip",pos+(3,0,0),ALLOW_REPLACE);
		}stop;
	}
}

const HDLD_MAUSER="bc9";
