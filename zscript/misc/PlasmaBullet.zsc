class HDB_ODD:HDBulletActor{ //Strange Plasma Bolts
	default{
		pushfactor 0.6;
        mass 25;
        speed 550;
        accuracy 270;
        stamina 560;
        woundhealth 1;
        hdbulletactor.hardness 1;
	}
	override actor Puff(){
		if(max(abs(pos.x),abs(pos.y))>=32768)return null;
		setorigin(pos-(2*(cos(angle),sin(angle)),0),false);
		A_SprayDecal("PlasmaShock",16);
		if(vel==(0,0,0))A_ChangeVelocity(cos(pitch),0,-sin(pitch),CVF_RELATIVE|CVF_REPLACE);
		else vel*=0.01;
		A_AlertMonsters();
		bmissile=false;
		if(!instatesequence(curstate,findstate("death")))setstatelabel("death");
		return null;
	} 
	override void onhitactor(actor hitactor,vector3 hitpos,vector3 vu,int flags){
		double bleed=0.4;
		double impact=(speed*speed*0.000001)*0.1*mass;
		double spbak=speed;
		super.onhitactor(hitactor,hitpos,vu,flags);
		if(spbak-speed>10)puff();
		if(hd_debug)console.printf(hitactor.getclassname().." resisted, impact:  "..impact);
		A_AlertMonsters();
	}
	states{
	spawn:
		SPLS AB 1 bright;
		loop;
	death:
		TNT1 A 1;
		stop;
	}
}

class HDB_PLAS:HDBulletActor{ //"Rifle Grade" Plasma Bolts
	default{
		pushfactor 0.6;
        mass 2;
        speed 750;
        accuracy 666;
        stamina 560;
        woundhealth 6;
        hdbulletactor.hardness 1;
	}
	states{
	spawn:
		PLSS AB 1 bright;
		loop;
	death:
		TNT1 A 1;
		stop;
	}
}
class HDB_PLASDMR:HDBulletActor{ //"Rifle Grade" Plasma Bolts
	default{
		pushfactor 0.6;
        mass 2;
        speed 1750;
        accuracy 666;
        stamina 560;
        woundhealth 6;
        hdbulletactor.hardness 1;
	}
	override actor Puff(){
		if(max(abs(pos.x),abs(pos.y))>=32768)return null;
		setorigin(pos-(2*(cos(angle),sin(angle)),0),false);
		A_SprayDecal("PlasmaShock",16);
		if(vel==(0,0,0))A_ChangeVelocity(cos(pitch),0,-sin(pitch),CVF_RELATIVE|CVF_REPLACE);
		else vel*=0.01;
		A_AlertMonsters();
		doordestroyer.destroydoor(self,frandom(1,frandom(16,40)),frandom(1,frandom(8,24)));
		actor aaa=Spawn("SmallestChunker",pos,ALLOW_REPLACE);
		DistantQuaker.Quake(self,2,35,256,12);
		bmissile=false;
		if(!instatesequence(curstate,findstate("death")))setstatelabel("death");
		return null;
	}
	override void onhitactor(actor hitactor,vector3 hitpos,vector3 vu,int flags){
		double impact=(speed*speed*0.0001)*0.1*mass;
		double spbak=speed;
		super.onhitactor(hitactor,hitpos,vu,flags);
		if(hd_debug)console.printf(hitactor.getclassname().." resisted, impact:  "..impact);
		actor aaa=Spawn("HDSmoke",pos,ALLOW_REPLACE);
		A_AlertMonsters();
		let hdmb=hdmobbase(hitactor);
		let shields=HDMagicShield(hitactor.findinventory("HDMagicShield"));
		if(hdmb&&shields&&shields.amount>0){
			hitactor.damagemobj(self,target,20,"thermal",DMG_FORCED);
			shields.amount-=20;
		}
		else hitactor.damagemobj(self,target,180,"thermal",DMG_FORCED); //dmg vs no shield
		if(hdmb&&shields&&shields.amount<160&&shields.amount>1){
			shields.amount=0;
			A_HDBlast(
				blastradius:256,blastdamage:120,
				immolateradius:24,immolateamount:24,immolatechance:44,
				source:target
			);
			actor aaa=Spawn("PlasXpl",pos,ALLOW_REPLACE);
				aaa=Spawn("PlasXpl",pos,ALLOW_REPLACE);
				aaa=Spawn("PlasXpl",pos,ALLOW_REPLACE);
				aaa=Spawn("SmallChunker",pos,ALLOW_REPLACE);
			DistantQuaker.Quake(self,3,35,256,12);
			A_StartSound("world/explode",CHAN_AUTO);
			hitactor.damagemobj(self,target,320,"thermal",DMG_FORCED); //dmg from pop
		}
	}
	//copying over the whole function from HDBulletActor just to remove the supersonic crack
	override void tick(){ 
		if(isfrozen())return;

		//if(getage()%17)return;  //debug line for when i_timescale isn't appropriate

		if(abs(realpos.x)>32000||abs(realpos.y)>32000){destroy();return;}
		if(
			!bmissile
		){
			bnoteleport=false;
			super.tick();
			return;
		}

		//I COULD put in the work to check for teleportation
		//but it would be a nightmare (bad) for gameplay purposes,
		//and super-slow bullets that can teleport are not really something that actually happens.
		if(!bnoteleport)bnoteleport=true;

		//update position but keep within the sector
		if(
			realpos.xy!=pos.xy
		)setorigin((
			realpos.xy,
			clamp(
				realpos.z,
				getzat(realpos.x,realpos.y,flags:GZF_ABSOLUTEPOS),
				getzat(realpos.x,realpos.y,flags:GZF_ABSOLUTEPOS|GZF_CEILING)-height
			)
		),true);

		tracelines.clear();
		traceactors.clear();
		tracesectors.clear();

		//if in the sky
		if(
			ceilingz<realpos.z
			&&ceilingz-realpos.z<vel.z
		){
			if(
				!(level.time&(1|2|4|8|16|32|64|128))
				&&(vel.xy dot vel.xy < 64.)
				&&!level.ispointinlevel(pos)
			){
				destroy();
				return;
			}
			bnointeraction=true;
			binvisible=true;
			realpos+=vel;
			ApplyDeceleration();
			ApplyGravity();
			return;
		}
		if(bnointeraction){
			bnointeraction=false;
			binvisible=false;
		}

		if(vel==(0,0,0)){
			vel.z-=max(0.01,getgravity()*0.01);
			return;
		}

		hdbullettracer blt=HDBulletTracer(new("HDBulletTracer"));
		if(!blt)return;
		blt.bullet=hdbulletactor(self);
		blt.shooter=target;
		vector3 oldpos=realpos;
		vector3 newpos=oldpos;

		//get speed, set counter
		bool doneone=false;
		double distanceleft=vel.length();
		double curspeed=distanceleft;
		do{
			A_FaceMovementDirection();
			tracer=null;

			//update distanceleft if speed changed
			if(curspeed>speed){
				distanceleft-=(curspeed-speed);
				curspeed=speed;
			}

			double cosp=cos(pitch);
			vector3 vu=vel.unit();
			blt.trace(
				realpos,
				cursector,
				vu,
				distanceleft,
				TRACE_HitSky
			);
			traceresults bres=blt.results;
			sector sectortodamage=null;


			//check distance until clear of target
			if(
				!bincombat
				&&(
					!target||
					bres.distance>target.height
				)
			){
				bincombat=true;
			}


			if(bres.hittype==TRACE_HasHitSky){
				realpos+=vel;
				ApplyDeceleration();
				ApplyGravity();
				newpos=bres.hitpos; //used to spawn crackers later
			}else if(bres.hittype==TRACE_HitNone){
				newpos=bres.hitpos;
				realpos=newpos;
				distanceleft-=max(bres.distance,10.); //safeguard against infinite loops
			}else{
				newpos=bres.hitpos-vu*0.1;
				realpos=newpos;
				distanceleft-=max(bres.distance,10.); //safeguard against infinite loops
				if(bres.hittype==TRACE_HitWall){
					setorigin(realpos,true);  //needed for bulletdie and checkmove

					let hitline=bres.hitline;
					tracelines.push(hitline);

					//get the sector on the opposite side of the impact
					sector othersector;
					if(bres.hitsector==hitline.frontsector)othersector=hitline.backsector;
					else othersector=hitline.frontsector;

					//check if the line is even blocking the bullet
					bool isblocking=(
						!(hitline.flags&line.ML_TWOSIDED) //one-sided
						||(
							//these barriers are not even paper thin
							hitline.flags&(
								line.ML_BLOCKHITSCAN
								|line.ML_BLOCKPROJECTILE
								|line.ML_BLOCKEVERYTHING
							)
						)
						//||bres.tier==TIER_FFloor //3d floor - does not work as of 4.2.0
						||hitline.gethealth()>0
						||( //upper or lower tier, not sky
							(
								(bres.tier==TIER_Upper)
								&&(othersector.gettexture(othersector.ceiling)!=skyflatnum)
							)||(
								(bres.tier==TIER_Lower)
								&&(othersector.gettexture(othersector.floor)!=skyflatnum)
							)
						)
						||!checkmove(bres.hitpos.xy+vu.xy*0.4) //if in any event it won't fit
					);

					// crossing or hitting impact activation line
					let activator = level.missilesActivateImpact? Actor(self) : target;
					hitline.activate(activator,bres.side,SPAC_Impact);

					//if not blocking, pass through and continue
					if(
						!isblocking
						||hitline.special==Line_Horizon
					){
						// crossing projectile line
						hitline.activate(self,bres.side,SPAC_PCross);

						realpos.xy+=vu.xy*0.2;
						setorigin(realpos,true);
					}else{
						//SPAC_Damage is already handled by the native geometry damage code called in HitGeometry
						HitGeometry(
							hitline,othersector,bres.side,999+bres.tier,vu,
							doneone?bres.distance:999
						);
						if(!self)return;
					}
				}else if(
					bres.hittype==TRACE_HitFloor
					||bres.hittype==TRACE_HitCeiling
				){
					sector hitsector=bres.hitsector;
					tracesectors.push(hitsector);

					setorigin(realpos,true);
					if(
						(
							bres.hittype==TRACE_HitCeiling
							&&(
								hitsector.gettexture(hitsector.ceiling)==skyflatnum
								||ceilingz>pos.z+0.1
							)
						)||(
							bres.hittype==TRACE_HitFloor
							&&(
								hitsector.gettexture(hitsector.floor)==skyflatnum
								||floorz<pos.z-0.1
							)
						)
					)continue;

					HitGeometry(
						null,hitsector,0,
						bres.hittype==TRACE_HitCeiling?SECPART_Ceiling:SECPART_Floor,
						vu,doneone?bres.distance:999
					);
					if(!self)return;
				}else if(bres.hittype==TRACE_HitActor){
					setorigin(realpos,true);
					if(
						bincombat
						||bres.hitactor!=target
					){
						traceactors.push(bres.hitactor);
						onhitactor(bres.hitactor,bres.hitpos,vu);
						if(!self)return;
					}
				}
			}
			doneone=true;

/*
			//find points close to players and spawn crackers
			//also spawn trails if applicable
			if(speed>256){
				vector3 crackpos=newpos;
				vector3 crackinterval=vu*BULLET_CRACKINTERVAL;
				int j=int(max(1,bres.distance*(1./BULLET_CRACKINTERVAL)));
				for(int i=0;i<j;i++){
					crackpos-=crackinterval;
					if(hd_debug>1)A_SpawnParticle("yellow",SPF_RELVEL|SPF_RELANG,
						size:12,
						xoff:crackpos.x-pos.x,
						yoff:crackpos.y-pos.y,
						zoff:crackpos.z-pos.z,
						velx:speed*cos(pitch)*0.001,
						velz:-speed*sin(pitch)*0.001
					);
					if(missilename)spawn(missilename,crackpos,ALLOW_REPLACE);
					bool gotplayer=false;

					bool supersonic=speed>HDCONST_SPEEDOFSOUND;
					double crackvol=(speed*10+(pushfactor*mass))*0.00004;
					double fwooshvol=crackvol*0.32;
					double crackpitch=clamp(speed*0.001,0.9,1.5);

					for(int k=0;!gotplayer && k<MAXPLAYERS;k++){
						if(playeringame[k] && players[k].mo){
							vector3 vvv=players[k].mo.pos-crackpos;  //vec3offset is wrong; portals don't work
							if(
								(vvv dot vvv)<(512*512)
							){
								gotplayer=true;
								actor ccc=spawn("BulletSoundTrail",crackpos,ALLOW_REPLACE);
								double thisvol=crackvol;
								do{
									if(supersonic)ccc.A_StartSound("weapons/bulletcrack",
										CHAN_BODY,CHANF_OVERLAP,volume:clamp(thisvol,0,1),
										attenuation:ATTN_STATIC,pitch:crackpitch
									);
									else ccc.A_StartSound("weapons/subfwoosh",
										CHAN_BODY,CHANF_OVERLAP,volume:clamp(fwooshvol,0,1),
										attenuation:ATTN_STATIC,pitch:crackpitch
									);
									if(thisvol>1)thisvol-=1;
								}while(thisvol>1);
							}
						}
					}
				}
			} */
		}while(
			bmissile
			&&distanceleft>0
		);

		//destroy the linetracer just in case it interferes with savegames
		blt.destroy();

		//update velocity
		double pf=min(pushfactor,speed*0.1);
		vel+=(
			frandom(-pf,pf),
			frandom(-pf,pf),
			frandom(-pf,pf)
		);
		//reduce momentum
		ApplyDeceleration();
		ApplyGravity();

		//sometimes bullets will freeze (or at least move imperceptibly slowly)
		//and not react to gravity or anything until touched.
		//i've never been able to isolate the cause of this.
		//this forces a bullet to die if its net movement is less than 1 in all cardinal directions.
		//(note: if a bullet is shot straight up and hangs perfectly still for a tick,
		//it's almost certainly "in the sky" and the below code would not be executed.
		//also consider grav acceleration: 32 speed straight up from height 0: +32+30+27+23+18+12+5-3..)
		if(
			abs(oldpos.x-realpos.x)<1
			&&abs(oldpos.y-realpos.y)<1
			&&abs(oldpos.z-realpos.z)<1
		)bulletdie();

		if(tics>=0)nexttic();
	}
}

class HDB_PLASMAG:HDB_PLAS{ //Overclocked Plasma Bolts
	override actor Puff(){
		if(max(abs(pos.x),abs(pos.y))>=32768)return null;
		setorigin(pos-(2*(cos(angle),sin(angle)),0),false);
		A_SprayDecal("PlasmaShock",16);
		if(vel==(0,0,0))A_ChangeVelocity(cos(pitch),0,-sin(pitch),CVF_RELATIVE|CVF_REPLACE);
		else vel*=0.01;
		A_AlertMonsters();
		A_HDBlast(
			blastradius:32,blastdamage:160,
			immolateradius:1,immolateamount:40,immolatechance:12,
			source:target
		);
		doordestroyer.destroydoor(self,frandom(1,frandom(32,80)),frandom(1,frandom(16,48))); //Oh fuck, it blasted a hole in my TV screen...
		actor aaa=Spawn("SmallChunker",pos,ALLOW_REPLACE);
		DistantQuaker.Quake(self,3,35,256,12);
		bmissile=false;
		if(!instatesequence(curstate,findstate("death")))setstatelabel("death");
		return null;
	}
	override void onhitactor(actor hitactor,vector3 hitpos,vector3 vu,int flags){
		double impact=(speed*speed*0.000001)*0.1*mass;
		double spbak=speed;
		super.onhitactor(hitactor,hitpos,vu,flags);
		if(hd_debug)console.printf(hitactor.getclassname().." resisted, impact:  "..impact);
		actor aaa=Spawn("HDSmoke",pos,ALLOW_REPLACE);
		A_AlertMonsters();
		let hdmb=hdmobbase(hitactor);
		let shields=HDMagicShield(hitactor.findinventory("HDMagicShield"));
		if(hdmb&&shields&&shields.amount>0){
			hitactor.damagemobj(self,target,20,"thermal",DMG_FORCED);
			shields.amount-=20;
		}else hitactor.damagemobj(self,target,250,"thermal",DMG_FORCED);//damage vs no shield
		if(hdmb&&shields&&shields.amount<400&&shields.amount>1){
			shields.amount=0;
			A_HDBlast(
				blastradius:256,blastdamage:100,
				immolateradius:24,immolateamount:24,immolatechance:44,
				source:target
			);
			actor aaa=Spawn("PlasXpl",pos,ALLOW_REPLACE);
				aaa=Spawn("PlasXpl",pos,ALLOW_REPLACE);
				aaa=Spawn("PlasXpl",pos,ALLOW_REPLACE);
				aaa=Spawn("SmallChunker",pos,ALLOW_REPLACE);
			DistantQuaker.Quake(self,3,35,256,12);
			A_StartSound("world/explode",CHAN_AUTO);
			hitactor.damagemobj(self,target,500,"thermal",DMG_FORCED); //dmg from pop
		}
	}
}

class PlasXpl:HDExplosion{
	states{
	spawn:
	death:
		TFOG B 0 nodelay{
			if(max(abs(pos.x),abs(pos.y),abs(pos.z))>=32768){destroy();return;}
			vel.z+=4;
			A_StartSound(deathsound,CHAN_BODY);
			let xxx=spawn("HDExplosionLight",pos);
			xxx.target=self;
		}
		TFOG BB 0 A_SpawnItemEx("ParticleWhiteSmall", 0,0,0, vel.x+random(-2,2),vel.y+random(-2,2),vel.z,0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPOINTERS);
		TFOG BBBB 0 A_SpawnItemEx("HDSmoke", 0,0,0, vel.x+frandom(-2,2),vel.y+frandom(-2,2),vel.z,0,SXF_ABSOLUTEMOMENTUM|SXF_NOCHECKPOSITION|SXF_TRANSFERPOINTERS);
		TFOG B 0 A_Jump(256,"fade");
	fade:
		TFOG B 1 A_FadeOut(0.1);
		TFOG C 1 A_FadeOut(0.2);
		TFOG DD 1 A_FadeOut(0.2);
		TNT1 A 20;
		stop;
	}
}
