// ------------------------------------------------------------
// Fulgur Laser Machinegun
// ------------------------------------------------------------
const HDLD_BEAMGUN="lsm";
const HD_BEAMLASERMAXRANGE=HDCONST_ONEMETRE*150;
const HD_BEAMLASERMAXFOCUS=15000;
const HD_BEAMLASERMINFOCUS=750;

class HDLaserMachinegun:HDCellWeapon{
	default{
		weapon.selectionorder 70;
		weapon.slotnumber 6;
		weapon.slotpriority 0.9;
		weapon.ammouse 1;
		scale 0.75;
		inventory.pickupmessage "You got the fulgur laser machinegun!";
		obituary "%o was hammered by %k's particle beam.";
		hdweapon.barrelsize 32,1.6,3;
		hdweapon.refid HDLD_BEAMGUN;
		tag "Fulgur Laser Machinegun";

		hdweapon.loadoutcodes "
			\cualt - 0/1, whether to start in manual focus mode";
	}
	override bool AddSpareWeapon(actor newowner){return AddSpareWeaponRegular(newowner);}
	override hdweapon GetSpareWeapon(actor newowner,bool reverse,bool doselect){return GetSpareWeaponRegular(newowner,reverse,doselect);}
	override void tick(){
		super.tick();
		drainheat(BMGS_HEAT,3);
		if(airburst < HD_BEAMLASERMINFOCUS)
		{
			airburst=HD_BEAMLASERMINFOCUS;
		}
		if(airburst > HD_BEAMLASERMAXFOCUS)
		{
			airburst=HD_BEAMLASERMAXFOCUS;
		}
	}
	override double gunmass(){
		return 12+(weaponstatus[BMGS_BATTERY]<0?0:2);
	}
	override double weaponbulk(){
		double bat1;
		double bat2;
		if(weaponstatus[BMGS_BATTERY]>=0) bat1 = ENC_BATTERY_LOADED;
		if(weaponstatus[BMGS_BATTERY2]>=0) bat2 = ENC_BATTERY_LOADED;
		return 190+bat1+bat2;
	}
	override string,double getpickupsprite(){return "LSMPA0",1.1;}
	override void DrawHUDStuff(HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl){
		if(sb.hudlevel==1){
			sb.drawbattery(-54,-4,sb.DI_SCREEN_CENTER_BOTTOM,reloadorder:true);
			sb.drawnum(hpl.countinv("HDBattery"),-46,-8,sb.DI_SCREEN_CENTER_BOTTOM);
		}
		
		if(!hdw.weaponstatus[0]&BMGF_ALT)
			sb.drawimage(
				"STBURAUT",(-16,-23),
				sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_TRANSLATABLE|sb.DI_ITEM_RIGHT
			);
		//sb.drawnum(int(2000/HDCONST_ONEMETRE),-19,-20,sb.DI_SCREEN_CENTER_BOTTOM,font.CR_GRAY);
		
		int bat=hdw.weaponstatus[BMGS_BATTERY];
		int bat2=hdw.weaponstatus[BMGS_BATTERY2];
		
		if(bat>0)sb.drawwepnum(bat,20, posy:-10);
		else if(!bat)sb.drawstring(
			sb.mamountfont,"000000",
			(-16,-14),sb.DI_TEXT_ALIGN_RIGHT|sb.DI_TRANSLATABLE|sb.DI_SCREEN_CENTER_BOTTOM,
			Font.CR_DARKGRAY
		);
		
		if(bat2>0)sb.drawwepnum(bat2,20);
		else if(!bat2)sb.drawstring(
			sb.mamountfont,"000000",
			(-16,-7),sb.DI_TEXT_ALIGN_RIGHT|sb.DI_TRANSLATABLE|sb.DI_SCREEN_CENTER_BOTTOM,
			Font.CR_DARKGRAY
		);
		
		int ab=hdw.airburst;
		sb.drawstring(
			sb.mAmountFont,ab?string.format("%.2f",hdw.airburst*0.01):"--.--",
			(-17,-21),sb.DI_SCREEN_CENTER_BOTTOM|sb.DI_TEXT_ALIGN_RIGHT,
			ab?Font.CR_WHITE:Font.CR_BLACK
		);
	}
	override string gethelptext(){
		return
		WEPHELP_FIRESHOOT
		..WEPHELP_ALTFIRE.."  Switch to "..(weaponstatus[0]&BMGF_ALT?"auto focus":"manual focus").." mode\n"
		..WEPHELP_FIREMODE.."+"..WEPHELP_UPDOWN.."  Manually adjust beam focus\n"
		..WEPHELP_RELOAD.."  Reload primary battery\n"
		..WEPHELP_UNLOAD.."  Unload primary battery\n"
		..WEPHELP_ALTRELOAD.."  Reload secondary battery\n"
		..WEPHELP_USE.."+"..WEPHELP_UNLOAD.."  Unload secondary battery\n"
		;
	}
	int rangefinder;
	override void DrawSightPicture(
		HDStatusBar sb,HDWeapon hdw,HDPlayerPawn hpl,
		bool sightbob,vector2 bob,double fov,bool scopeview,actor hpc
	){
		int cx,cy,cw,ch;
		[cx,cy,cw,ch]=screen.GetClipRect();
		sb.SetClipRect(
			-16+bob.x,-4+bob.y,32,16,
			sb.DI_SCREEN_CENTER
		);
		vector2 bobb=bob*1.2;
		sb.drawimage(
			"tbfrntsit",(0,0)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP
		);
		sb.SetClipRect(cx,cy,cw,ch);
		sb.drawimage(
			"tbbaksit",(0,0)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
			alpha:0.9
		);
		/*if(!scopeview)
		{
			let tb=HDLaserMachinegun(hdw);
			sb.drawnum(min(tb.rangefinder,999),
				20+bob.x,5+bob.y,sb.DI_SCREEN_CENTER,
				(
					tb.rangefinder<=HD_BEAMLASERMAXRANGE/HDCONST_ONEMETRE
					&&tb.rangefinder<hdw.weaponstatus[BMGS_MAXRANGEDISPLAY]
				)?Font.CR_GRAY:Font.CR_RED
				,0.2
			);
		}//*/

		if(scopeview){
			int scaledyoffset=36;
			bool alt = 0;

			name ctex="HDXCAM_TB";

			texman.setcameratotexture(hpc,ctex,3);
			sb.drawimage(
				ctex,(0,scaledyoffset)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
				alpha:alt?(hpl.flip?0.7:0.8):1.,scale:((0.25/1.2),0.25)
			);
			let tb=HDLaserMachinegun(hdw);
			sb.drawnum(min(tb.rangefinder,999),
				24+bob.x,12+bob.y,sb.DI_SCREEN_CENTER,
				(
					tb.rangefinder<=HD_BEAMLASERMAXRANGE/HDCONST_ONEMETRE
					&&tb.rangefinder<hdw.weaponstatus[BMGS_MAXRANGEDISPLAY]
				)?Font.CR_GRAY:Font.CR_RED
				,0.4
			);
			sb.drawnum(int(HD_BEAMLASERMAXRANGE/HDCONST_ONEMETRE),
				24+bob.x,19+bob.y,sb.DI_SCREEN_CENTER,Font.CR_WHITE,0.5
			);
			sb.drawimage(
				"tbwindow",(0,scaledyoffset)+bob,sb.DI_SCREEN_CENTER|sb.DI_ITEM_CENTER,
				scale:(1,1)
			);
			bobb*=3;
			double dotoff=max(abs(bobb.x),abs(bobb.y));
			if(dotoff<40)sb.drawimage(
				"redpxl",(0,scaledyoffset)+bobb,sb.DI_SCREEN_CENTER|sb.DI_ITEM_TOP,
				alpha:(alt?0.4:0.9)*(1.-dotoff*0.04),scale:alt?(hpl.flip?(3,3):(1,1)):(2,2)
			);
		}
	}
	override void failedpickupunload(){
		failedpickupunloadmag(BMGS_BATTERY,"HDBattery");
		failedpickupunloadmag(BMGS_BATTERY2,"HDBattery");
	}
	override void consolidate(){
		CheckBFGCharge(BMGS_BATTERY);
		CheckBFGCharge(BMGS_BATTERY2);
	}
	static void LaserZap(
		actor caller,
		double zoffset=32,
		int battery=20,
		double sm1=0,
		double sm2=0,
		double focus=300 
	){
		//determine angle
		double shootangle=caller.angle;
		double shootpitch=caller.pitch;
		vector3 shootpos=(0,0,zoffset);
		let hdp=hdplayerpawn(caller);
		if(hdp){
			shootangle=hdp.gunangle;
			shootpitch=hdp.gunpitch;
			shootpos=hdp.gunpos;
		}
		shootangle+=sm1;
		shootpitch+=sm2;

		//create the line
		flinetracedata tlt;
		caller.linetrace(
			shootangle,
			8000+200*battery,
			shootpitch,
			flags:TRF_NOSKY|TRF_ABSOFFSET,
			offsetz:shootpos.z,
			offsetforward:shootpos.x,
			offsetside:shootpos.y,
			data:tlt
		);


		if(
			tlt.hittype==Trace_HitNone
			||(
				tlt.hitline&&(
					tlt.hitline.special==Line_Horizon
					||(
						tlt.linepart==2
						&&tlt.hitsector.gettexture(0)==skyflatnum
					)||(
						tlt.linepart==1
						&&tlt.hitline.sidedef[1]
						&&hdmath.oppositesector(tlt.hitline,tlt.hitsector).gettexture(0)==skyflatnum
					)
				)
			)
		)return;

		//alt does a totally different thing
		if(tlt.hittype==Trace_HitNone||tlt.distance>HD_BEAMLASERMAXRANGE)return;
		actor bbb=spawn("BeamGunSpotFlash",tlt.hitlocation-tlt.hitdir,ALLOW_REPLACE);
		bbb.setz(clamp(bbb.pos.z,bbb.floorz,bbb.ceilingz-8));
		if(!random(0,3))(lingeringthunder.zap(bbb,bbb,caller,40,true));
		
		double fuck;
		if(tlt.distance > focus)
		{
			fuck = tlt.distance - focus;
		}
		else if(tlt.distance <= focus)
		{
			fuck = focus - tlt.distance;
		}
		BeamGunSpotFlash(bbb).impactdistance=clamp(fuck, 0, 1500);//-16*battery;
		if(hd_debug)
		{caller.a_logint(fuck);
		caller.a_logint(tlt.distance);}
		
		//BeamGunSpotFlash(bbb).impactdistance=tlt.distance-16*battery;
		bbb.angle=caller.angle;
		bbb.A_SprayDecal("Scorch",12);
		bbb.pitch=caller.pitch;
		bbb.target=caller;
		bbb.tracer=tlt.hitactor; //damage inflicted on the puff's end
		if(tlt.hitline)doordestroyer.CheckDirtyWindowBreak(tlt.hitline,0.005*battery,tlt.hitlocation);
		
		double dist = int(tlt.Distance);	
		actor laser = Spawn("BeamGunLaser", caller.pos+shootpos-(0,0,1));
		BeamGunLaser(laser).angle = shootangle;
		BeamGunLaser(laser).pitch = shootpitch*-1 - 90;
		BeamGunLaser(laser).scale.y = dist/112;//clamp(dist, 0, 128);*/
		BeamGunLaser(laser).alpha = 1.0 - clamp(fuck, 0, 800)*0.001;
		
		actor fx = Spawn("BeamGunLaserFX", caller.pos+shootpos-(0,0,1));
		BeamGunLaserFX(fx).alpha = 1.0 - clamp(fuck, 0, 800)*0.001;
		
		Spawn("BeamSpotFlashLight", caller.pos+shootpos-(0,0,1));
		hdp.A_StopSound(CHAN_7);
		hdp.A_StartSound("weapons/lasermgfire", CHAN_WEAPON);
	}
	
	action void A_LaserZap(double m1 = 0, double m2 = 0, bool otherbat = 0, bool usefocus = 1){
		if(invoker.weaponstatus[BMGS_HEAT]>80)return;
		int battery;
		int focus;
		if(otherbat == TRUE) battery=invoker.weaponstatus[BMGS_BATTERY2];
		else battery=invoker.weaponstatus[BMGS_BATTERY];
		if(battery<1){
			setweaponstate("nope");
			return;
		}
		if(usefocus == TRUE) focus = invoker.airburst*HDCONST_ONEMETRE*0.01;
		if(usefocus == FALSE) focus = int(500*HDCONST_ONEMETRE*0.01);

		//preliminary effects
		A_StartSound("weapons/lasermgidle", CHAN_7);
		A_ZoomRecoil(0.99);
		if(countinv("IsMoving")>9)A_MuzzleClimb(frandom(-0.8,0.8),frandom(-0.8,0.8));

		//the actual call
		HDLaserMachinegun.LaserZap(
			self,
			gunheight(),
			battery,
			m1,
			m2,
			focus
		);

		//aftereffects
		if(!random(0,6))
		{
			if(otherbat == TRUE)invoker.weaponstatus[BMGS_BATTERY2]--;
			else invoker.weaponstatus[BMGS_BATTERY]--;
		}
		A_MuzzleClimb(
			frandom(0.05,0.2),frandom(-0.2,-0.4),
			frandom(0.1,0.3),frandom(-0.2,-0.6)//,
			//frandom(0.04,0.12),frandom(-0.1,-0.3),
			//frandom(0.01,0.03),frandom(-0.1,-0.2)
		);
		invoker.weaponstatus[BMGS_HEAT]+=random(5,6);

		//update range thingy
		invoker.weaponstatus[BMGS_MAXRANGEDISPLAY]=int(
			(battery>0?battery*200+8000:0)/HDCONST_ONEMETRE
		);
	}
	
	action void A_FocusAdjust(){
		A_WeaponReady(WRF_NONE);
		//int minfocus=int(HD_BEAMLASERMINFOCUS)
		int iab=invoker.airburst;
		int cab=0;
		int mmy=GetMouseY(true);
		if(justpressed(BT_ATTACK))cab=HD_BEAMLASERMINFOCUS;
		else if(justpressed(BT_ALTATTACK))cab=HD_BEAMLASERMINFOCUS;
		else if(mmy){
			cab=-mmy;
			if(abs(cab)>(1<<1))cab>>=1;else cab=clamp(cab,-1,1);
		}
		iab+=cab;
		if(iab<HD_BEAMLASERMINFOCUS){
			if(cab>0)iab=HD_BEAMLASERMINFOCUS;
			else iab=0;
		}
		if(iab>HD_BEAMLASERMAXFOCUS){
			iab=HD_BEAMLASERMAXFOCUS;
		}
		invoker.airburst=clamp(iab,HD_BEAMLASERMINFOCUS,HD_BEAMLASERMAXFOCUS);
	}
	
	action void A_UpdateRangefinder()
	{
		vector3 gunpos=gunpos();
		flinetracedata frt;
		linetrace(
			angle,
			512*HDCONST_ONEMETRE,
				pitch,
				flags:TRF_NOSKY|TRF_ABSOFFSET,
				offsetz:gunpos.z,
				offsetforward:gunpos.x,
				offsetside:gunpos.y,
				data:frt
			);
			if(
				frt.hittype==Trace_HitNone
				||(
					frt.hittype==Trace_HitCeiling
					&&frt.hitsector.gettexture(1)==skyflatnum
				)||(
					frt.hittype==Trace_HitFloor
					&&frt.hitsector.gettexture(0)==skyflatnum
				)||(
					!!frt.hitline
					&&frt.hitline.special==Line_Horizon
				)
			)invoker.rangefinder=999;
			else 
			{
				invoker.rangefinder=int(frt.distance*(1./HDCONST_ONEMETRE));
			}
	}
	
	states{
	ready:
		LSRG A 1{
			//update rangefinder
			if(
				!(level.time&(1|2|4))
				/*&&max(abs(vel.x),abs(vel.y),abs(vel.z))<2
				&&(
					!player.cmd.pitch
					&&!player.cmd.yaw
				)*/
			){
				A_UpdateRangefinder();
			}
			int rframe = random(1,4);
			player.GetPSprite(PSP_WEAPON).frame = rframe;
			A_WeaponReady(WRF_ALL);
		}goto readyend;
		LSRG ABCDE 0;
		LSRF AB 0;
	firemode:
		#### A 1 A_FocusAdjust();
		---- A 0 A_JumpIf(pressingfiremode(),"firemode");
		goto readyend;
	fire:
		#### A 0 offset(0,35)
		{
			if(!invoker.weaponstatus[0]&BMGF_ALT) 
			{
				A_SetTics(3);
				A_UpdateRangefinder();
				invoker.airburst=invoker.rangefinder*100+random(invoker.rangefinder*-20,invoker.rangefinder*20);
			}
		}
	hold:
		#### A 0 A_JumpIf(invoker.weaponstatus[BMGS_BATTERY2]>0,"shoot");
		#### A 0 A_JumpIf(invoker.weaponstatus[BMGS_BATTERY]>0,"shoot");
		goto nope;
	shoot:
		#### B 1 offset(1,33) 
		{
			if(!invoker.weaponstatus[0]&BMGF_ALT)
			{
				double fuck = invoker.weaponstatus[BMGS_HEAT]*0.008;
				fuck = clamp(fuck, 0, 0.4);
				double inacc = frandom(-fuck, fuck);
				
				if(invoker.weaponstatus[BMGS_BATTERY]>0)A_LaserZap(inacc, inacc);
				else if(invoker.weaponstatus[BMGS_BATTERY2]>0)A_LaserZap(inacc, inacc, 1);
			}
			else
			{
				double fuck = invoker.weaponstatus[BMGS_HEAT]*0.008;
				fuck = clamp(fuck, 0, 0.3);
				double inacc = frandom(-fuck, fuck);
				
				if(invoker.weaponstatus[BMGS_BATTERY]>0)A_LaserZap(inacc, inacc);
				else if(invoker.weaponstatus[BMGS_BATTERY2]>0)A_LaserZap(inacc, inacc, 1);
			}
			A_GunFlash();
		}
		#### C 1 offset(0,34) A_WeaponReady(WRF_NONE);
		#### D 1 offset(-1,33) A_WeaponReady(WRF_NONE);
		#### E 1 offset(0,32) A_WeaponReady(WRF_NONE);
		//#### E 1 offset(0,32) A_WeaponReady(WRF_NONE);
		#### A 0{
			if(invoker.weaponstatus[BMGS_BATTERY]<0 && invoker.weaponstatus[BMGS_BATTERY2]<0)
			{
					A_StartSound("weapons/lasermgs",CHAN_WEAPON);
					//A_GunFlash();
					setweaponstate("nope");
			}
			else{
				A_Refire();
			}
		}
		#### AAAAA 1{
			A_WeaponReady(WRF_NOFIRE);
		}goto ready;
	flash:
		LSRF A 0;
		#### B 2 bright{
			HDFlashAlpha(64);
			A_Light2();
		}
		#### A 2 bright A_Light1();
		#### A 0 bright A_Light0();
		stop;
		
	altfire:
		#### A 1 offset(1,32) A_WeaponBusy();
		#### A 2 offset(2,32);
		#### A 1 offset(1,33) A_StartSound("weapons/lasermgswitch",8);
		#### A 2 offset(0,34);
		#### A 3 offset(-1,35);
		#### A 4 offset(-1,36);
		#### A 3 offset(-1,35);
		#### A 2 offset(0,34){
			invoker.weaponstatus[0]^=BMGF_ALT;
			A_SetHelpText();
		}
		#### A 1;
		#### A 1 offset(0,34);
		#### A 1 offset(1,33);
		goto nope;

	select0:
		LSRG A 0{
			invoker.weaponstatus[BMGS_MAXRANGEDISPLAY]=int(
				(8000+200*invoker.weaponstatus[BMGS_BATTERY])/HDCONST_ONEMETRE
			);
		}goto select0big;
	deselect0:
		LSRG A 0;
		#### A 0 A_Light0();
		goto deselect0big;

	unload:
		#### A 0{
			invoker.weaponstatus[0]|=BMGF_JUSTUNLOAD;
			if(player.cmd.buttons&BT_USE && invoker.weaponstatus[BMGS_BATTERY2]>=0)
				return resolvestate("unmag2");
			if(invoker.weaponstatus[BMGS_BATTERY]>=0)
				return resolvestate("unmag");
			return resolvestate("nope");
		}goto magout;
	unmag:
		#### A 2 offset(0,33){
			A_SetCrosshair(21);
			A_MuzzleClimb(frandom(-1.2,-2.4),frandom(1.2,2.4));
		}
		#### A 3 offset(0,35) A_StartSound("weapons/lasermgopen",8);
		#### A 6 offset(0,40) A_StartSound("weapons/lasermgload",8,CHANF_OVERLAP);
		#### A 0{
			int bat=invoker.weaponstatus[BMGS_BATTERY];
			A_MuzzleClimb(frandom(-1.2,-2.4),frandom(1.2,2.4));
			if(
				(
					bat<0
				)||(
					!PressingUnload()&&!PressingReload()
				)
			)return resolvestate("dropmag");
			return resolvestate("pocketmag");
		}

	dropmag:
		---- A 0{
			int bat=invoker.weaponstatus[BMGS_BATTERY];
			invoker.weaponstatus[BMGS_BATTERY]=-1;
			if(bat>=0){
				HDMagAmmo.SpawnMag(self,"HDBattery",bat);
			}
		}goto magout;

	pocketmag:
		---- A 0{
			int bat=invoker.weaponstatus[BMGS_BATTERY];
			invoker.weaponstatus[BMGS_BATTERY]=-1;
			if(bat>=0){
				HDMagAmmo.GiveMag(self,"HDBattery",bat);
			}
		}
		#### A 8 offset(0,43) A_StartSound("weapons/pocket",9);
		#### A 8 offset(0,42) A_StartSound("weapons/pocket",9,CHANF_OVERLAP);
		goto magout;

	magout:
		---- A 0 A_JumpIf(invoker.weaponstatus[0]&BMGF_JUSTUNLOAD,"reload3");
		goto loadmag;

	reload:
		#### A 0{
			invoker.weaponstatus[0]&=~BMGF_JUSTUNLOAD;
			if(
				invoker.weaponstatus[BMGS_BATTERY]<20
				&&countinv("HDBattery")
			)setweaponstate("unmag");
		}goto nope;

	loadmag:
		#### A 2 offset(0,43){
			A_StartSound("weapons/pocket",9);
			if(health>39)A_SetTics(0);
		}
		#### AA 2 offset(0,42);
		#### A 2 offset(0,44) A_StartSound("weapons/pocket",9);
		#### A 4 offset(0,43);
		#### A 6 offset(0,42);
		#### A 8 offset(0,38)A_StartSound("weapons/lasermgload",8);
		#### A 4 offset(0,37){if(health>39)A_SetTics(0);}
		#### A 4 offset(0,36)A_StartSound("weapons/lasermgclose",8);

		#### A 0{
			let mmm=HDMagAmmo(findinventory("HDBattery"));
			if(mmm)invoker.weaponstatus[BMGS_BATTERY]=mmm.TakeMag(true);
		}goto reload3;

	reload3:
		#### A 6 offset(0,40){
			invoker.weaponstatus[BMGS_MAXRANGEDISPLAY]=int(
				(8000+200*invoker.weaponstatus[BMGS_BATTERY])/HDCONST_ONEMETRE
			);
			A_StartSound("weapons/lasermgclose2",8);
		}
		#### A 2 offset(0,36);
		#### A 4 offset(0,33);
		goto nope;
		
	//ATTACK OF THE COPY PASTED STATES
	//admittedly this gun is a huge hacked mess solely because i decided to
	//slice up the tb compared to making a thing mostly from scratch like with
	//the plasma pistol lol
	
	unmag2:
		#### A 2 offset(0,33){
			A_SetCrosshair(21);
			A_MuzzleClimb(frandom(-1.2,-2.4),frandom(1.2,2.4));
		}
		#### A 3 offset(0,35) A_StartSound("weapons/lasermgopen",8);
		#### A 6 offset(0,40) A_StartSound("weapons/lasermgload",8,CHANF_OVERLAP);
		#### A 0{
			int bat=invoker.weaponstatus[BMGS_BATTERY2];
			A_MuzzleClimb(frandom(-1.2,-2.4),frandom(1.2,2.4));
			if(
				(
					bat<0
				)||(
					!PressingUnload()&&!PressingReload()
				)
			)return resolvestate("dropmag2");
			return resolvestate("pocketmag2");
		}

	dropmag2:
		---- A 0{
			int bat=invoker.weaponstatus[BMGS_BATTERY2];
			invoker.weaponstatus[BMGS_BATTERY2]=-1;
			if(bat>=0){
				HDMagAmmo.SpawnMag(self,"HDBattery",bat);
			}
		}goto magout2;

	pocketmag2:
		---- A 0{
			int bat=invoker.weaponstatus[BMGS_BATTERY2];
			invoker.weaponstatus[BMGS_BATTERY2]=-1;
			if(bat>=0){
				HDMagAmmo.GiveMag(self,"HDBattery",bat);
			}
		}
		#### A 8 offset(0,43) A_StartSound("weapons/pocket",9);
		#### A 8 offset(0,42) A_StartSound("weapons/pocket",9,CHANF_OVERLAP);
		goto magout2;

	magout2:
		---- A 0 A_JumpIf(invoker.weaponstatus[0]&BMGF_JUSTUNLOAD,"reload3");
		goto loadmag2;

	altreload:
		#### A 0{
			invoker.weaponstatus[0]&=~BMGF_JUSTUNLOAD;
			if(
				invoker.weaponstatus[BMGS_BATTERY2]<20
				&&countinv("HDBattery")
			)setweaponstate("unmag2");
		}goto nope;

	loadmag2:
		#### A 2 offset(0,43){
			A_StartSound("weapons/pocket",9);
			if(health>39)A_SetTics(0);
		}
		#### AA 2 offset(0,42);
		#### A 2 offset(0,44) A_StartSound("weapons/pocket",9);
		#### A 4 offset(0,43);
		#### A 6 offset(0,42);
		#### A 8 offset(0,38)A_StartSound("weapons/lasermgload",8);
		#### A 4 offset(0,37){if(health>39)A_SetTics(0);}
		#### A 4 offset(0,36)A_StartSound("weapons/lasermgclose",8);

		#### A 0{
			let mmm=HDMagAmmo(findinventory("HDBattery"));
			if(mmm)invoker.weaponstatus[BMGS_BATTERY2]=mmm.TakeMag(true);
		}goto reload3;

	user3:
		#### A 0 A_MagManager("HDBattery");
		goto ready;

	spawn:
		LSMP A -1;
		stop;
	}
	override void initializewepstats(bool idfa){
		//weaponstatus[0]=1;
		weaponstatus[BMGS_BATTERY]=20;
		weaponstatus[BMGS_BATTERY2]=20;
		if(!idfa && !owner){
			airburst=HD_BEAMLASERMINFOCUS;
		}
	}
	override void loadoutconfigure(string input){
		int fm=getloadoutvar(input,"alt",1);
		if(!fm)weaponstatus[0]&=~BMGF_ALT;
		else if(fm>0)weaponstatus[0]|=BMGF_ALT;
	}
}
enum beamgungunstatus{
	BMGF_ALT=1,
	BMGF_JUSTUNLOAD=2,

	BMGS_FLAGS=0,
	BMGS_BATTERY=1,
	BMGS_BATTERY2=2,
	BMGS_HEAT=3,
	BMGS_MAXRANGEDISPLAY=4
};

//alt fire puff
class BeamGunSpotFlash:IdleDummy{
	default{
		+puffonactors +hittracer +puffgetsowner +rollsprite +rollcenter +forcexybillboard
		renderstyle "add";
		obituary "%o was hammered by %k's particle splatter.";
		decal "Scorch";
		scale 0.5;
	}
	double impactdistance;
	override void postbeginplay(){
		if(impactdistance>HD_BEAMLASERMAXRANGE){
			destroy();
			return;
		}
		super.postbeginplay();
		
		double impactcloseness=2000 - impactdistance * 2;
		scale*=(impactcloseness)*0.0006;
		alpha=scale.y+0.3;

		int n=int(max(impactcloseness*0.03,2));
		int n1=n*3/5;
		int n2=n*2/5;
		if(tracer){
			HDF.Give(tracer,"Heat",n);
			int dmgflags=target&&target.player?DMG_PLAYERATTACK:0;
			tracer.damagemobj(self,target,random(n1,n),"electrical",dmgflags);
		}
		A_HDBlast(
			n*2,random(1,n),n,"electrical",
			n,-n,
			immolateradius:n1,immolateamount:random(4,8)*n2/-10,immolatechance:n
		);

		pitch=frandom(80,90);
		angle=frandom(0,360);
		A_SpawnItemEx("BeamSpotFlashLight",flags:SXF_NOCHECKPOSITION|SXF_SETTARGET);
		A_SpawnChunks("HDGunSmoke",clamp(n2*3/5,4,7),3,6);
		A_StartSound("weapons/lasermgimpact");
		A_AlertMonsters();
	}
	states{
	spawn:
		BBB1 AB 2 bright;
		BBB1 CD 2 bright A_FadeIn(0.1);
		BBB1 E 2 bright A_FadeOut(0.1);
		stop;
	}
}

class BeamGunLaser : Actor
{	
	Default
	{
		+BRIGHT
		+NOINTERACTION
		+MASKROTATION
		VisibleAngles -180,180;
		VisiblePitch -180,180;
		RenderRadius 2048;
		XScale 0.5;
		RenderStyle "Add";
	}
	
	States
	{
		Spawn:
			TBLZ A 0 NODELAY {bINTERPOLATEANGLES = TRUE;}
			TBLZ A 1;
			TBLZ AAAA 1
			{
				if(scale.x > 0.1) A_SetScale(scale.x-0.1, scale.y);
				A_FadeOut(0.1);
			}
			Stop;
	}
}

class BeamGunLaserFX : Actor
{	
	Default
	{
		+BRIGHT
		+NOINTERACTION
		+ROLLSPRITE
		+ROLLCENTER
		+FORCEXYBILLBOARD
		Scale 0.3;
		RenderStyle "Add";
	}
	
	States
	{
		Spawn:
			TBLZ B 0 NODELAY {bINTERPOLATEANGLES = TRUE; frame=random(1,6);}
			#### # 1;
			#### #### 1
			{
				A_SetScale(scale.x-0.05);
				A_FadeOut(0.1);
			}
			Stop;
	}
}
