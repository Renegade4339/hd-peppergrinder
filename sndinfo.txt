weapons/lotusbang				LOTFIR		//Courtesy of 1337spy
weapons/lotusquiet				LOTSIL		//Courtesy of 1337spy
$alias weapons/lotusopen		weapons/deinoopen
$alias weapons/lotusclose		weapons/deinoclose
$alias weapons/lotusload		weapons/deinoload
$alias weapons/lotuseject		weapons/deinoeject
$alias weapons/lotusclick		weapons/deinoclick
$alias weapons/lotuscyl			weapons/deinocyl $volume weapons/lotuscyl 0.3

weapons/otisblast1  OTISFIRE
weapons/otisblast2  dsshtfar
weapons/otisclick   OTISDRYF
weapons/otiseject   OTISEJEC
weapons/otiscyl     OTISCOCK  $volume weapons/otiscyl 0.3
weapons/otisload    OTISINSR
weapons/otisopen    OTISOPEN
weapons/otisclose   OTISCLOS

$alias weapons/lotusblast1		weapons/deinoblast1
$alias weapons/lotusblast2		weapons/deinoblast2
$alias weapons/lotusclick		weapons/deinoclick
$alias weapons/lotuseject		weapons/deinoeject
$alias weapons/lotuscyl			weapons/deinocyl
$alias weapons/lotusload		weapons/deinoload
$alias weapons/lotusopen		weapons/deinoopen
$alias weapons/lotusclose		weapons/deinoclose

weapons/bcfire   			MOUSER		//Courtesy of 1337spy
$alias weapons/bcquiet		weapons/lotusquiet $volume weapons/bcquiet 0.35
$alias weapons/bcload		weapons/rifleload
$alias weapons/bcclick		weapons/rifleclick	
$alias weapons/bcclick2		weapons/rifleclick2

$alias weapons/trog     weapons/rifle   $limit weapons/trog 0 //shootbang

$alias weapons/trogclick    weapons/rifleclick		//used for manually chambering new round (including during reload)
$alias weapons/trogclick2     weapons/rifleclick2		//used during reload
$alias weapons/trogunload  	   weapons/rifleload					//magout
$alias weapons/trogload        weapons/rifleload		//magin
$alias weapons/troggrenopen    weapons/grenopen	//used for opening up the gl
$alias weapons/troggrenload    weapons/grenreload	//loading the grenade in
$alias weapons/trogchamber     weapons/rifchamber 	//chambering a new round??? i didn't check how this was different from click

$random weapons/hlar     { weapons/hlar1 weapons/hlar2 weapons/hlar3 }   $limit weapons/hlar 0 //shootbang
$random weapons/hlargrenadeshot	{ weapons/hlargl1 weapons/hlargl2 }

weapons/hlarunload  	   ar1mgout					
weapons/hlarload        ar1mgin		
$alias weapons/hlargrenopen    weapons/grenopen	
$alias weapons/hlargrenload    weapons/grenreload	
weapons/hlarchamber     ar1bolt 	
weapons/hlar1		ar1hks1
weapons/hlar2		ar1hks2
weapons/hlar3		ar1hks3
weapons/hlargl1	ar1gl1
weapons/hlargl2		ar1gl2

weapons/rubycharge					MASECHAR	//Deux Ex (2000, Ion Storm)
weapons/rubyendlo					MASELOWE	//Deux Ex (2000, Ion Storm)
weapons/rubyendhi					MASEEND		//Deux Ex (2000, Ion Storm)
weapons/rubybeam					MASEFIR1	//Deux Ex (2000, Ion Storm)
weapons/rubyfirelo					MASELOWF	//Deux Ex (2000, Ion Storm)
weapons/rubyfirehi					MASEFIR2	//Deux Ex (2000, Ion Storm)
weapons/rubymaserpoweron			MASEPOW1	//Deux Ex (2000, Ion Storm)
weapons/rubymaserpoweroff			MASEPOW2	//Deux Ex (2000, Ion Storm)
//all the beam sounds will break without these
$pitchshift weapons/rubyfirelo		0
$limit weapons/rubyfirelo			0
$pitchshift weapons/rubyfirehi		0
$limit weapons/rubyfirehi			0
$limit weapons/rubybeam				0

weapons/hmpholy					DSSHINE			//Courtesy of 1337spy
weapons/hmpfire					HELZFIRE		//Courtesy of 1337spy
weapons/hmpfireholy				HELZFRHL		//Courtesy of 1337spy
$alias weapons/hmpblast			weapons/deinoblast2
$alias weapons/hmpchamber1		weapons/pischamber1
$alias weapons/hmpchamber2		weapons/pischamber2
$alias weapons/hmpmagclick		weapons/pismagclick
$alias weapons/bottleload		potion/swish

//It's just the gunshot from CarnEvil
weapons/greelyshot						CARNSHOT $volume weapons/greelyshot 3.0//technically not a shotgun sound, but cmon I gotta
$alias weapons/greelyblast				weapons/deinoblast2
$alias weapons/greelyreload				weapons/huntreload
$alias weapons/greelyrackup				weapons/huntrackup
$alias weapons/greelyrackdown			weapons/huntrackdown

weapons/guillotine GUILFIRE
$volume weapons/guillotine 0.8
weapons/guilmagout GUILMAGO
weapons/guilmagin  GUILMAGI
$alias weapons/guildry    weapons/pistoldry
weapons/guilchamber1 GUILCHAM
$alias weapons/guilchamber2  weapons/guilchamber1

weapons/vera 						VERAFIRE
$alias weapons/verabelt			weapons/vulcbelt
$alias weapons/veramag			weapons/vulcmag
$alias weapons/veraumag		weapons/vulcumag
$alias weapons/verachamber	weapons/vulcchamber
$alias weapons/veraanup			weapons/vulcanup
$alias weapons/veraandown		weapons/vulcandown
$alias weapons/veraanon			weapons/vulcanon
$alias weapons/verashunt		weapons/vulcshunt
$alias weapons/veraopen1		weapons/vulcopen1
$alias weapons/veraopen2		weapons/vulcopen2
$alias weapons/veraextract		weapons/vulcextract
$alias weapons/veraforcemag	weapons/vulcforcemag
$random weapons/verafix {weapons/verashunt weapons/veraforcemag weapons/veraumag weapons/verabelt}
$random weapons/veratryfix {weapons/verashunt weapons/veraforcemag}
$random weapons/veratryfix2 {weapons/verashunt weapons/veraopen1 weapons/veraumag weapons/veraforcemag weapons/veraextract weapons/bigcrack weapons/pocket}

weapons/wiseaufire			wisfire		//Courtesy of 1337spy
$alias weapons/wiseauload	weapons/pischamber2
$alias weapons/wiseauzap	misc/arczap

Oddball/NormalShot				ODBF1
Oddball/WeakShot				ODBF2
OddBall/Crank					ODBSPN
Oddball/Woopsie					ODBFUP
OddBall/PumpUp					ODPUP
OddBall/PumpDown				ODPDN

$alias weapons/lisazap			misc/arczap
$alias weapons/lisafire			weapons/wiseaufire $volume weapons/lisafire 0.45
$alias weapons/lisaload			weapons/pischamber2
weapons/lisacharge				LISBEEP
weapons/lisafullcharge			LISBEPF

weapons/spacelube					bstslip
weapons/bastardfire					BASTARDF
$alias weapons/bastardload			weapons/trogload
$alias weapons/bastardclick			weapons/trogclick
$alias weapons/bastardclick2		weapons/trogclick2
$alias weapons/bastgrenopen			weapons/troggrenopen
$alias weapons/bastgrenload			weapons/troggrenload

weapons/bp90fire							p90fire	//Courtesy of 1337spy
weapons/redcrownfire						REDCROWN
$alias weapons/bp90chamber					weapons/rifchamber
$alias weapons/bp90unload					weapons/trogunload
$alias weapons/bp90load						weapons/trogload
$alias weapons/bp90click					weapons/trogclick
$alias weapons/bp90click2					weapons/trogclick2

$alias weapons/maulerfwoosh weapons/bfgfwoosh
$alias weapons/maulidle weapons/plasidle
$alias weapons/maulopen weapons/plasopen
$alias weapons/maulload weapons/plasload
$alias weapons/maulcharge weapons/bfgcharge
$alias weapons/maulf weapons/bfgf
$alias weapons/maulclose weapons/plasclose
$alias weapons/maulclose2 weapons/plasclose2
$alias weapons/maulsf weapons/plasmaf
$alias weapons/maulfar weapons/bfgfar
$alias weapons/maulx weapons/bfgx
$limit weapons/maulx 0
$alias weapons/maulsprayfire weapons/bfgfwoosh
$volume weapons/maulx 0.15
$alias weapons/maultx weapons/bfgx

$alias weapons/sawedslayersingle weapons/slayersingle
$alias weapons/sawedsshoto		 weapons/sshoto
$alias weapons/sawedsshotl		 weapons/sshotl
$alias weapons/sawedsshotc		 weapons/sshotc

//i forgot that its "weapons/" and not "weapon/" while making it, oh well
weapon/plpistolfire1 "sounds/PlasmaPistol/plasma_rifle_fire_plasmarifle1.wav"
weapon/plpistolfire2 "sounds/PlasmaPistol/plasma_rifle_fire_plasmarifle2.wav"
weapon/plpistolfire3 "sounds/PlasmaPistol/plasma_rifle_fire_plasmarifle3.wav"
weapon/plpistolfire4 "sounds/PlasmaPistol/plasma_rifle_fire_plasmarifle4.wav"
weapon/plpistolfire5 "sounds/PlasmaPistol/plasma_rifle_fire_plasmarifle5.wav"

$random weapon/plpistolfire
{
	weapon/plpistolfire1
	weapon/plpistolfire2
    weapon/plpistolfire3
    weapon/plpistolfire4
    weapon/plpistolfire5
}

weapon/plpistolhit1 "sounds/PlasmaPistol/plasma_rifle_plasmahit_plasma_hit1.wav"
weapon/plpistolhit2 "sounds/PlasmaPistol/plasma_rifle_plasmahit_plasma_hit2.wav"
weapon/plpistolhit3 "sounds/PlasmaPistol/plasma_rifle_plasmahit_plasma_hit3.wav"

$random weapon/plpistolhit
{
	weapon/plpistolhit1
	weapon/plpistolhit2
    weapon/plpistolhit3
}

weapon/plpistolcrackle1 "sounds/PlasmaPistol/plasma_rifle_sparks_plasma_sparks_cracklelp2.wav"
weapon/plpistolcrackle2 "sounds/PlasmaPistol/plasma_rifle_sparks_plasma_sparks_cracklelp3.wav"
weapon/plpistolcrackle3 "sounds/PlasmaPistol/plasma_rifle_sparks_plasma_sparks_cracklelp4.wav"
weapon/plpistolcrackle4 "sounds/PlasmaPistol/plasma_rifle_sparks_plasma_sparks_cracklelp5.wav"

$random weapon/plpistolcrackle
{
	weapon/plpistolcrackle1
	weapon/plpistolcrackle2
    weapon/plpistolcrackle3
    weapon/plpistolcrackle4
    weapon/plpistolcrackle5
}

weapon/plpistolwoosh1 "sounds/PlasmaPistol/plasma_rifle_overheat_overheat1.wav"
weapon/plpistolwoosh2 "sounds/PlasmaPistol/plasma_rifle_overheat_overheat2.wav"
weapon/plpistolwoosh3 "sounds/PlasmaPistol/plasma_rifle_overheat_overheat3.wav"

$random weapon/plpistolwoosh
{
	weapon/plpistolwoosh1
	weapon/plpistolwoosh2
    weapon/plpistolwoosh3
}

weapon/plpistolcharge 				"sounds/PlasmaPistol/plasma_rifle_charge_newchrgloop2.ogg"
weapon/plpistolchargefire 			"sounds/PlasmaPistol/plasma_rifle_chargefire.wav"
weapon/plpistolchargebeep 			"sounds/PlasmaPistol/plaspistol_beep.wav"

weapon/plpistoloverheat 			"sounds/PlasmaPistol/plaspistol_overheat.wav"
weapon/plpistoloverheatsmall		"sounds/PlasmaPistol/plaspistol_overheat_small.ogg"
weapon/plpistolsteam 				"sounds/PlasmaPistol/steam_loop22_02.wav"

weapon/plpistolfire_marathon 		"sounds/PlasmaPistol/marathon_fire.wav"
weapon/plpistolhit_marathon 		"sounds/PlasmaPistol/marathon_hit.wav"
weapon/plpistolcharge_marathon 		"sounds/PlasmaPistol/marathon_charge.wav"
weapon/plpistolchargefire_marathon 	"sounds/PlasmaPistol/marathon_chargefire.wav"


weapons/chinalake CHINLKFR
weapons/chinalakepumpback CHINLKPB
weapons/chinalakepumpforward CHINLKPF


$random weapons/chinalakeload {
weapons/chinalakeload1
weapons/chinalakeload2
weapons/chinalakeload3
}

weapons/chinalakeload1 CHNLINS1
weapons/chinalakeload2 CHNLINS2
weapons/chinalakeload3 CHNLINS3

weapons/bpx	 		 	BPXGFIRE
weapons/bpxmagout	 	BPXGMAGO
weapons/bpxmagin	 	BPXGMAGI
weapons/bpxboltback	 	BPXGBLTB
weapons/bpxboltforward	BPXGBLTF

$alias misc/zorcport misc/teleport
$alias weapons/zorcx weapons/bfgx
$alias weapons/zorcclose2 weapons/plasclose2
$alias weapons/zorcclose weapons/plasclose
$alias weapons/zorcload weapons/plasload
$alias weapons/zorcfire weapons/plasmaf

weapons/zm94fire sounds/ZM94/dsz94fir.ogg
$random weapons/zm94fix {weapons/vulcshunt weapons/vulcforcemag weapons/vulcumag weapons/smack}
$alias weapons/zm94tryfix weapons/veratryfix
$alias weapons/zm94tryfix1 weapons/veratryfix1
$alias weapons/zm94tryfix2 weapons/veratryfix2
$alias weapons/zm94belt weapons/smack
$alias weapons/zm94open1 weapons/veraopen1
$alias weapons/zm94open2 weapons/veraopen2
$alias weapons/zm94magout weapons/rifleload
$alias weapons/zm94magin weapons/rifleload
$alias weapons/zm94click weapons/rifleclick
$alias weapons/zm94click2 weapons/rifleclick2
$alias weapons/zm94chamber2 weapons/libchamber2
$alias weapons/zm94chamber2a weapons/libchamber2a
$alias weapons/zm94boltback weapons/libchamber
$alias weapons/zm94boltforw weapons/libchamber

//Fulgur
$alias weapons/lasermgidle weapons/plasidle
$alias weapons/lasermgs weapons/plasmas
$alias weapons/lasermgswitch weapons/plasswitch
$alias weapons/lasermgopen weapons/plasopen
$alias weapons/lasermgclose weapons/plasclose
$alias weapons/lasermgclose2 weapons/plasclose2
weapons/lasermgfire "sounds/Fulgur/lasermachinegun_1.ogg"
weapons/lasermgimpact "sounds/Fulgur/w_plasma200_bullet_hit.wav"
$limit weapons/lasermgimpact 0


weapons/auto5fire AUT5FIRE

$random weapons/auto5reload {
weapons/auto5reload1
weapons/auto5reload2
weapons/auto5reload3
}

weapons/auto5reload1	AUT5LOD1
weapons/auto5reload2	AUT5LOD2
weapons/auto5reload3	AUT5LOD3


weapons/auto5boltback	AUT5BBCK
weapons/auto5boltforward	AUT5BFOR


weapons/auryshoot		auryfire
weapons/auryshotgun		AURYFRSH
weapons/aurypull 		AURYPULL
weapons/auryrotate		AURYCLIK
weapons/auryopen		auryopen
weapons/auryclose		auryclos
weapons/auryeject		auryejec
weapons/auryempty		AURYEMPT

$random weapons/auryinsert {weapons/auryinsert1 weapons/auryinsert2 weapons/auryinsert3}
weapons/auryinsert1		auryins1
weapons/auryinsert2		auryins2
weapons/auryinsert3		auryins3

$random weapons/auryshellinsert {
weapons/auryshellinsert1
weapons/auryshellinsert2
weapons/auryshellinsert3
}

weapons/auryshellinsert1		AURSHIN1
weapons/auryshellinsert2		AURSHIN2
weapons/auryshellinsert3		AURSHIN3

thebox/charge	AMCNCHRG
thebox/eject	AMCNEJEC

weapons/dmrfire 		DMRFFIRE
$volume weapons/dmrfire 0.67
weapons/dmrmagout		DMRFMAGO
weapons/dmrmagin		DMRFMAGI
weapons/dmrboltback		DMRFBLTB
weapons/dmrboltforward	DMRFBLTF

weapons/spectresmg "sounds/YureiSMG/spectre_fire.wav"
$volume weapons/spectresmg 0.4
$limit weapons/spectresmg 0

//Special thanks to 1337spy, who does a shitload of sound design work on weapons and did weapon sounds for HDest prpper.
